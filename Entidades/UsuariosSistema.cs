﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class UsuariosSistema
    {
        public UsuariosSistema()
        {             
        }

        public string Identificacion { get; set; }
        public int IdTipoDocumento { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public int Profesion { get; set; }
        public int Relacion { get; set; }
        public int TelefonoFijo { get; set; }
        public long TelefonoCelular { get; set; }
        public string CorreoElectronico { get; set; }
        public string usuario { get; set; }
        public string Clave { get; set; }
        public int Rol { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaUltimaActualizacion { get; set; }
        public string UsuarioRegistro { get; set; }
        public string UsuarioUltimaActualizacion { get; set; }
    }
}
