﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class _Default : Page
    {
        Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter ControladorUsuario = new Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            string Mensaje = "";
            if (Session["Origen"] != null)
            {
                string[] Origen = (string[])Session["Origen"];
                if (Origen[0] == "Acceso")
                {
                    Mensaje = Mensaje + Origen[1] + Environment.NewLine;
                    plnNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    lblTitulo.Text = "Error";
                    Session["Origen"] = null;

                }
            }
            if (Session["IdOrigen"] != null)
            {
                if (((string[])Session["IdOrigen"])[0] == "RegistroUser")
                {
                    Mensaje = Mensaje + ((string[])Session["IdOrigen"])[1] + Environment.NewLine;
                    plnNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    plnControlador.Attributes.Add("class", "alert alert-success alert-dismissible fade show");
                    lblTitulo.Text = "Creación exitosa del usuario";
                    Session["IdOrigen"] = null;
                }
            }
        }


        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            if (txtIdentificacion.Text == "")
            {
                string Mensaje = "Debe ingresar la Identificación del usuario!!";
                plnNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                lblTitulo.Text = "Error";
                Mensaje = "";
            }
            else
            {
                if (Session["Login"] != null)
                {
                    if (Convert.ToInt32(((DataTable)Session["Login"]).Rows[0][11]) < 3)
                    {
                        string Mensaje = "No tiene los permisos necesarios para acceder a los datos de este usuario, contacte al administrador!";
                        plnNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        lblTitulo.Text = "Error";
                        Mensaje = "";
                    }
                    else
                    {
                        DataTable Usuario = ControladorUsuario.Tabla_ConsultarUsuario(txtIdentificacion.Text);
                        if (Usuario != null)
                        {
                            if (Usuario.Rows.Count > 0)
                            {

                                //Debe devolver al perfil del usuario
                                //Response.Redirect("U_RegistroUsuario.aspx");
                                //Debe retornar a Usuario
                                Session["IdMaster"] = txtIdentificacion.Text;
                                Response.Redirect("U_Usuario.aspx");
                            }
                            else
                            {
                                string Mensaje = "El Usuario no se encuentra registrado en el sistema!";
                                plnNotificaciones.Visible = true;
                                lblMensaje.Text = Mensaje;
                                Mensaje = "";
                                lblTitulo.Text = "Error";
                            }
                        }
                        else
                        {
                            string Mensaje = "El Usuario no se encuentra registrado en el sistema!";
                            plnNotificaciones.Visible = true;
                            lblMensaje.Text = Mensaje;
                            lblTitulo.Text = "Error";
                            Mensaje = "";
                        }
                    }
                }
                else
                {
                    string[] Origen = new string[2];
                    Origen[0] = "Inicio";
                    Origen[1] = txtIdentificacion.Text.Trim();
                    Session["IdMaster"] = txtIdentificacion.Text;
                    Session["IdOrigen"] = Origen;
                    Response.Redirect("A_Acceso.aspx");

                }
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            if (Session["Login"] == null)
            {
                string[] Origen = new string[2];
                Origen[0] = "Nuevo";
                Session["IdOrigen"] = Origen;
                Response.Redirect("A_Acceso.aspx");
            }
            else
            {
                if (Convert.ToInt32(((DataTable)Session["Login"]).Rows[0][11]) < 3)
                {
                    string Mensaje = "No tiene los permisos necesarios para registrar un usuario, contacte al administrador!";
                    plnNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    lblTitulo.Text = "Error";
                    Mensaje = "";
                }
                else
                {
                    Session["IdMaster"] = null;
                    Response.Redirect("U_RegistroUsuario.aspx");
                }
            }
        }

    }
}