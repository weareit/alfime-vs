﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SeguimientoControl.aspx.cs" Inherits="WebApplication1.Formulario_web16" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <link href="Content/assets/css/aristo-ui.css" rel="stylesheet" />
    <link href="Content/assets/css/materialize.min.css" rel="stylesheet" />
    <script src="Content/assets/css/jquery-2.1.1.min.js"></script>
    <script src="Content/assets/css/materialize.min.js"></script>    
    <link href="Content/assets/css/material-dashboard.min.css" rel="stylesheet" />
    <script>
    
        $(document).ready(function(){
    $('.tabs').tabs();
  });
    </script>
    <script type='text/JavaScript'>       

     function verocultar(cual) {
          var c=cual.nextSibling;
          if(c.style.display=='none') {
               c.style.display='block';
          } else {
               c.style.display='none';
          }
          return false;
        }

        function moverseA(idDelElemento) {
            document.getElementById(idDelElemento).click();
        }

        function muestra_oculta(id){
            if (document.getElementById) {                
            var el = document.getElementById(id); 
                el.style.display = (el.style.display == 'none') ? 'block' : 'none'; 
                 
            }
        } 
        
       
</script>

    <div id="pnlNotificaciones" runat="server" visible="false" >
        <div runat="server" id="pnlContenedorNotificaciones" class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong><asp:Label ID="lblTituloMensaje" runat="server">Error</asp:Label></strong> <asp:Label runat="server" Text="" Id="lblMensaje"></asp:Label>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12" runat="server" id="plnComplementarios" enabled="false">
                    <div class="card">
                        <div class="row">
                            <h3 style="margin-left: 30px">Paciente:
                                <asp:Label ID="lblpaciente" runat="server" Text="Consulte Un paciente!"></asp:Label>
                            </h3>
                        </div>
                        <div class="card-header card-header-tabs card-header-warning">
                            <%-----------------------------Cabecera de Tabs---------------------------%>

                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">
                                    <span class="nav-tabs-title"></span>
                                    <ul class="nav nav-tabs" data-tabs="tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#Evolucion" data-toggle="tab">
                                                <i class="material-icons">history</i> Evolución del Paciente                           
                                              <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#Domicilio" data-toggle="tab">
                                                <i class="material-icons">home</i> Gestión Domiciliaria                           
                                              <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#SocioCultural" data-toggle="tab">
                                                <i class="material-icons">accessibility</i> Gestión Socio Cultural                           
                                              <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#Acondicionamiento" data-toggle="tab">
                                                <i class="material-icons">accessible_forward</i> Acondicionamiento y Habilidades                          
                                              <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <%---------------------------Fin Cabecera de Tabs---------------------------%>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" runat="server" id="pnlGeneral">
                                <div class="tab-pane active" id="Evolucion">
                                    <br style ="margin-top:-10px" />
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="card">
                                                <div class="card-header card-header-success">
                                                    <h5 class="card-title">Evolución</h5>
                                                </div>
                                                <div class="card-body">
                                                    <table>
                                                        <tr>
                                                            <td>Área o Servicio</td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlCita" class="form-control">
                                                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                                                    <asp:ListItem Value="Fisioterapia" Text="Fisioterapia"></asp:ListItem>
                                                                    <asp:ListItem Value="Psicología:" Text="Psicología:"></asp:ListItem>
                                                                    <asp:ListItem Value="Terapia Familiar" Text="Terapia Familiar"></asp:ListItem>
                                                                    <asp:ListItem Value="Educación Especial" Text="Educación Especial"></asp:ListItem>
                                                                    <asp:ListItem Value="Deporte Adaptado" Text="Deporte Adaptado"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Fecha de control:</td>
                                                            <td>
                                                                <asp:TextBox ID="dtpFechaCita" TextMode="Date" class="form-control" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Diagnóstico:</td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtDiagnostico" TextMode="MultiLine" Style="border: 1px solid gray; border-radius: 3px 4px"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Evolución:</td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtEvolución" TextMode="MultiLine" Style="border: 1px solid gray; border-radius: 3px 4px"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Profesional Responsable:</td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlProfesional" class="form-control">
                                                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                                                </asp:DropDownList></td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="text-align: center">
                                                                <asp:Button ID="btnRegistrarEVO" runat="server" CssClass="btn btn-info" Text="Registrar Evolución" />
                                                                <asp:Button ID="btnCancelarEVO" runat="server" CssClass="btn btn-warning" Text="Cancelar" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="card">
                                                <div class="card-header card-header-success">
                                                    <h5 class="card-title">Evoluciones Registradas</h5>
                                                </div>
                                                <div class="card-body">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label runat="server" Text="Sin Evoluciones Registradas" ID="lblcitas"> </asp:Label>
                                                                <asp:GridView runat="server" ID="grvCita" class="table table-full-width table-bordered" AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                                                                        <asp:BoundField DataField="Profesional" HeaderText="Profesional" />
                                                                        <asp:BoundField DataField="Servicio" HeaderText="Servicio" />
                                                                        <asp:BoundField DataField="Diagnostico" HeaderText="Diagnóstico" />
                                                                        <asp:BoundField DataField="Evolucion" HeaderText="Evolución" />
                                                                        <asp:CommandField SelectText="Eliminar" ButtonType="Image" SelectImageUrl="~/Content/Iconos/error.png" ShowSelectButton="True">
                                                                            <ItemStyle Width="50px" />
                                                                            <HeaderStyle Width="50px" />
                                                                        </asp:CommandField>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="thead-dark" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane active" id="Domicilio">
                                    <br style ="margin-top:-10px" />
                                </div>

                                <div class="tab-pane active" id="SocioCultural">
                                    <br style ="margin-top:-10px" />
                                </div>

                                <div class="tab-pane active" id="Acondicionamiento">
                                    <br style ="margin-top:-10px" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
