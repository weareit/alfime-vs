﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="A_Acceso.aspx.cs" Inherits="WebApplication1.Formulario_web1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <%--Notificaciones--%>
    <div id="pnlNotificaciones" runat="server" visible="false">
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Error</strong> <asp:Label runat="server" Text="" Id="lblMensaje"></asp:Label>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>

     <%--Notificaciones--%>
    <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-warning card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">person</i>
                        </div>
                        <p class="card-title" style="font-size:24px">Acceso a la Gestión de Usuarios</p>

                    </div>
                    <div class="card-text" style="margin-top:30px">

                        <div class="form-group">                            
                            <div class="col-md-12">
                                <asp:TextBox ID="txtUsuario" runat="server" class="form-control"   MaxLength="100" placeholder="Usuario" ></asp:TextBox>                               
                            </div>
                        </div>

                        <div class="form-group">                           
                            <div class="col-md-12">
                                <asp:TextBox ID="txtClave" runat="server" class="form-control"  type="password" MaxLength="100" placeholder="Contraseña" ></asp:TextBox>
                                
                            </div>
                        </div>

                        <div class="form-group" style="text-align:right">
                            <div class="col-md-offset-2 col-md-12">                                
                                <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" class="btn btn-primary btn-round" OnClick="btnIngresar_Click" />

                                <asp:Button ID="btnRegistrar" runat="server" Text="Registrarme" class="btn btn-success btn-round" OnClick="btnRegistrar_Click" />
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-danger">warning</i>
                        <a href="#">Recuperar Contraseña</a>
                    </div>
                </div>
            </div>
        </div>
        
</asp:Content>
