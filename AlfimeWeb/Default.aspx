﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<div class="container-fluid">

        <%--Notificaciones--%>
    <div id="plnNotificaciones" runat="server" visible="false">
        <div id="plnControlador" runat="server" class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong><asp:Label runat="server" ID="lblTitulo"></asp:Label></strong> <asp:Label runat="server" Text="" Id="lblMensaje"></asp:Label>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>    
        <div class="row">
            <!-- Contenerdor central -->
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-warning card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">content_copy</i>
                                </div>
                                <p class="card-category"></p>                                
                                <h3> 49/50</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons text-danger">warning</i>
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="card card-stats">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">store</i>
                                </div>
                                <p class="card-category"></p>
                                <h3> 49/50</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="material-icons text-danger">date_range</i>
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <!--Fila de gráficos-->
                <div class="row">
                   
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title">ALFIME, ¿qué? ¿Quién? ¿Cúando?</h4>
                                    <p class="card-category">Un poco de Nuestra historia...</p>
                                </div>
                                <div class="card-body">
                                    <article class="card-description" style="color:dimgray">
                                         
                                        <br />
                                        <p style="text-align:center">Nuestra fundadora <b><span style="font-style:italic; color:black">ROSMIRA AVAREZ DE BOTERO.</span></b> Eran los albores del año 1987, así iniciaron la cita habitual en la cual
                                        departían un alegre café, hacían manualidades y charlas sobre diferentes temas, el grupo crecía y el espacio se reducía. 
                                        Buscaron sitios más amplios.</p>

                                        <p style="text-align:justify">Iniciaba el año 1988 cuando nos constituimos jurídicamente y logramos la primera sede, donde hoy es la Cámara 
                                        de Comercio de Envigado, a través de un comodato con el municipio, desde entonces funcionamos en diferentes sitios 
                                        de la ciudad, siempre casa en: las Margaritas, en el transito viejo, por el monumento de la madre, en el barrio 
                                        Andalucía, en los antigüos bomberos y ahora en esta sede que vamos transformando en el hogar que siempre soñamos 
                                        para la familia ALFIME en el antigüo centro de salud Israel Rendon. 28 años de labores, son el aleteo de una 
                                        mariposa en el universo y así entretejemos los hilos que forman la vida, sabemos que el corazón no tiene 
                                        discapacidad, todos podemos ser felices si lo entendemos como la ausencia de ansias de felicidad y una 
                                        tranquilidad no provista de tensión.</p>

                                        <p style="text-align:justify">
                                            Después de muchos ires y venires llegamos a lo que Para ALFIME es Vida Independiente: es la posibilidad de 
                                            que las personas con discapacidad ejerzan el poder de decisión sobre su propia existencia y participen 
                                            activamente de la vida en comunidad, asuman el derecho al libre desarrollo de la personalidad, la vida 
                                            particular y social bajo los principios de igualdad de oportunidades y no discriminación. La posibilidad de 
                                            llevar a cabo una vida independiente es el principal objeto de ALFIME.
                                        </p>

                                        <p>Actualmente tenemos diferentes Programas:</p> 
                                        <ul>
                                            <li>FISIOTERAPIA</li>
                                            <li>HIDROTERAPIA</li>
                                            <li>ACONDICIONAMIENTO FISICO</li>
                                            <li>PSICOLOGIA</li>
                                            <li>CLUB DEPORTIVO (NATACION, BOCCIA, TENIS DE MESA, TIRO DEPORTIVO, ATLETISMO)</li>
                                            <li>ARTES (CHIRIMIA COLOMBIA PA´TE CUMBIA, PERCUSION, TALLERES ARTISTICOS)</li>
                                            <li>PROGRAMA PEDAGOGICO Y SOCIO-OCUPACIONAL</li>
                                            <li>INSERCION LABORAL (ADMINISTRACION DE PARQUEADEROS)</li>
                                            <li>ASESORIA JURIDICA</li>
                                            <li>TURISMO SOCIAL</li>
                                            <li>RECREACION</li>
                                            <li>PARTICIPACION CIUDADANA</li>
                                            <li>PROCESO DE VIDA INDEPENDIENTE</li>
                                            <li>ASESORIA EN ACCESIBILIDAD UNIVERSAL PARA OBRAS DE INFRAESTRUCTURA PUBLICA Y PRIVADA</li>
                                        </ul>
                                        <br />
                                        <p>PROCESOS COMUNITARIOS E INCLUSION SOCIAL: </p>
                                        <p>Aceptación de equidad de derechos y reconocimiento de las habilidades y talentos de la población 
                                        con discapacidad, a través de un proceso social.</p>
                                        
                                        <p>Comunicate con nosotros al telefono: <b>3310541</b></p>

                                        <p>Dirección: <b>Dg. 30a #34dd Sur-39, Envigado, Antioquia, Colombia.</b></p>

                                        <p>Y en nuestras redes sociales <a href="https://www.facebook.com/pages/category/Local-Business/ALFIME-132269738179/">ALFIME facebook.</a></p>
                                    </article>
                                </div>
                            </div>
                        </div>                      
                    

                </div>

            </div>

            <!-- Contenerdor Usuario -->
            <div class="col-md-4">
                <div class="card card-profile">
                    <div class="card-avatar">
                        <img class="img" src="Content/assets/img/faces/marc.jpg" />
                     
                    </div>
                    <div class="card-body">
                        <h6 class="card-category text-gray" >Consultar Usuario</h6>
                        <div class="form-group">
                            <label style="margin: 0 auto" class="bmd-label-floating">Identificación Usaurio</label>
                            <asp:TextBox ID="txtIdentificacion" runat="server" title="Ingrese identificación del Usuario" class="form-control"></asp:TextBox>
                            
                        </div>
                        <asp:Button ID="btnConsultar" runat="server" Text="Consultar" OnClick="btnConsultar_Click" class="btn btn-primary btn-round"/>
                        
                        <br/>
                        
                        <p style="margin-top:10px" class="card-description">
                            Si el usuario no se encuentra registrado, recuerde validar sus datos y agregarlo al sistema...
                        </p>
                        <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" OnClick="btnNuevo_Click" class="btn btn-info btn-round"/>
                                              
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
