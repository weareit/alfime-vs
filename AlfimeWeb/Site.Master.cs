﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Datos;
using Entidades;

namespace WebApplication1
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cadena = HttpContext.Current.Request.Url.AbsoluteUri;
           

            
            if (Session["Login"] != null)
            {
                DataTable user = (DataTable)Session["Login"];
                lblAcceso.Visible = false;
                lblUsuario.Visible = true;
                string perfil = user.Rows[0][2].ToString() + Environment.NewLine + user.Rows[0][3].ToString();
                lblPerfil.Text = perfil.ToUpper();
                lblPerfil.ForeColor = Color.Blue;
                lblSalir.Visible = true;
                if (!cadena.Contains("Default"))
                {
                    pnlBuscar.Visible = true;
                }
                else
                {
                    pnlBuscar.Visible = false;
                }
                AccesoRol();


            }
            else
            {
                pnlBuscar.Visible = false;
                
                
            }
        }

        void AccesoRol()
        {
            if (Session["Login"] != null)
            {
                DataTable user = (DataTable)Session["Login"];
                int rol = Convert.ToInt32(user.Rows[0][11]);

                if (rol == 1)
                {
                    list6.Visible = true;
                }
                else if (rol == 2)
                {
                    list3.Visible = true;
                    list4.Visible = true;
                    list6.Visible = true;
                }
                else if (rol == 3)
                {
                    list2.Visible = true;
                    list3.Visible = true;
                    list4.Visible = true;
                    list5.Visible = true;
                    list6.Visible = true;
                }
                else if (rol == 4)
                {                    
                    list2.Visible = true;
                    list3.Visible = true;
                    list4.Visible = true;
                    list5.Visible = true;
                    list6.Visible = true;
                }


            }
        }

        protected void btnConsultarPaciente_ServerClick(object sender, EventArgs e)
        {
            if (txtIdPaciente.Text.ToString() != "")
            {
                string cadena = HttpContext.Current.Request.Url.AbsoluteUri;
                if (cadena.Contains("_Usuario"))
                {
                    Session["IdMaster"] = txtIdPaciente.Text;
                    Response.Redirect("U_Usuario.aspx");
                }
                if (cadena.Contains("Seguimiento"))
                {
                    Session["IdMaster"] = txtIdPaciente.Text;
                    Response.Redirect("SeguimientoControl.aspx");
                }
            }

        }

        protected void btnNuevoPaciente_ServerClick(object sender, EventArgs e)
        {
           
                Session["IdMaster"] = null;
                Response.Redirect("U_RegistroUsuario.aspx");
            
        }
    }
}