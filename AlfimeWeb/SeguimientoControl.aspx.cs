﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Formulario_web16 : System.Web.UI.Page
    {
        #region Acceso Datos y Constructores
        Datos.DataSetBDAlfimeTableAdapters.TipoDocumentoesTableAdapter ControladorTipoDoc = new Datos.DataSetBDAlfimeTableAdapters.TipoDocumentoesTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.EPSTableAdapter ControladorEPS = new Datos.DataSetBDAlfimeTableAdapters.EPSTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter ControladorUsuario = new Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.InfoGeneraUsuarioTableAdapter ControladorInfoGeneral = new Datos.DataSetBDAlfimeTableAdapters.InfoGeneraUsuarioTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.TipoViviendaTableAdapter ControladorTipoVivienda = new Datos.DataSetBDAlfimeTableAdapters.TipoViviendaTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.ParentescoTableAdapter ControladorParentesco = new Datos.DataSetBDAlfimeTableAdapters.ParentescoTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.FondoPensionesTableAdapter ControladorFP = new Datos.DataSetBDAlfimeTableAdapters.FondoPensionesTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.CajasCompesacionTableAdapter ControladorCC = new Datos.DataSetBDAlfimeTableAdapters.CajasCompesacionTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.Nivel_AcademicoTableAdapter ControladorNA = new Datos.DataSetBDAlfimeTableAdapters.Nivel_AcademicoTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.SeguridadSocialTableAdapter ControladorSS = new Datos.DataSetBDAlfimeTableAdapters.SeguridadSocialTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.DepartamentoSelectCommandTableAdapter ControladorDep = new Datos.DataSetBDAlfimeTableAdapters.DepartamentoSelectCommandTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.MunicipioSelectCommandTableAdapter ControladorMun = new Datos.DataSetBDAlfimeTableAdapters.MunicipioSelectCommandTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.ContactoUsuarioTableAdapter ControladorCU = new Datos.DataSetBDAlfimeTableAdapters.ContactoUsuarioTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.FamiliarTableAdapter ControladorFamiliares = new Datos.DataSetBDAlfimeTableAdapters.FamiliarTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.SituacionFinancieraTableAdapter ControladorFinanzas = new Datos.DataSetBDAlfimeTableAdapters.SituacionFinancieraTableAdapter();
        Datos.dsAlfimeAspectosTableAdapters.AspectoLaboralTableAdapter ControladorAL = new Datos.dsAlfimeAspectosTableAdapters.AspectoLaboralTableAdapter();
        Datos.dsAlfimeAspectosTableAdapters.AspectoMedicoTableAdapter ControladorAM = new Datos.dsAlfimeAspectosTableAdapters.AspectoMedicoTableAdapter();
        Datos.dsAlfimeAspectosTableAdapters.AspectoSicologicoTableAdapter ControladorAS = new Datos.dsAlfimeAspectosTableAdapters.AspectoSicologicoTableAdapter();
        Datos.dsAlfimeAspectosTableAdapters.AspectoVidaDiariaTableAdapter ControladorAV = new Datos.dsAlfimeAspectosTableAdapters.AspectoVidaDiariaTableAdapter();

        string Mensaje = string.Empty;
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] == null)
            {
                string cadena = HttpContext.Current.Request.Url.AbsoluteUri;
                if (!cadena.Contains("Default"))
                {
                    Response.Redirect("A_Acceso.aspx");
                }
            }
            if (!IsPostBack)
            {
                pnlGeneral.Visible = false;

                if (Session["IdMaster"] != null)
                {
                    CargarDatosPaciente((string)Session["IdMaster"]);
                    //Session["Usuario"] = null;
                    //Session["IdMaster"] = null;
                }
                if (Session["Usuario"] != null)
                {
                    DataTable Usu = (DataTable)Session["Usuario"];
                    CargarDatosPaciente(Usu.Rows[0]["Identificacion"].ToString());
                }



            }
        }

        void CargarDatosPaciente(string identificacion)
        {

            DataTable dtPaciente = ControladorUsuario.dtObtenerPaciente(identificacion);

            if (dtPaciente != null && dtPaciente.Rows.Count > 0)
            {

                pnlGeneral.Visible = true;
                lblpaciente.Text =
                    dtPaciente.Rows[0]["PrimerNombre"].ToString().ToUpper() + " " + dtPaciente.Rows[0]["SegundoNombre"].ToString().ToUpper() + " " +
                     dtPaciente.Rows[0]["PrimerApellido"].ToString().ToUpper() +" "+
                     dtPaciente.Rows[0]["SegundoApellido"].ToString().ToUpper();

            }
            else
            {
                string Mensaje = "No éxiste el Paciente por favor valide la identificación o Registrelo!";
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                lblTituloMensaje.Text = "Error";
                Mensaje = "";
            }

        }
    }
}