﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Datos;
using Entidades;

namespace WebApplication1
{
    public partial class Formulario_web11 : System.Web.UI.Page
    {
        Datos.DataSetBDAlfimeTableAdapters.TipoDocumentoesTableAdapter ControladorTipoDoc = new Datos.DataSetBDAlfimeTableAdapters.TipoDocumentoesTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.ProfesionTableAdapter ControladorProfesion = new Datos.DataSetBDAlfimeTableAdapters.ProfesionTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.RelacionTableAdapter ControladorRelacion = new Datos.DataSetBDAlfimeTableAdapters.RelacionTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.UsuarioSistemasTableAdapter ControladorUsuarioS = new Datos.DataSetBDAlfimeTableAdapters.UsuarioSistemasTableAdapter();


        protected void Page_Load(object sender, EventArgs e)
        {
            ddlTipoDocumento.DataSource = ControladorTipoDoc.GetData();
            ddlTipoDocumento.DataTextField = "Nombre";
            ddlTipoDocumento.DataValueField = "Id";
            ddlTipoDocumento.DataBind();

            ddlProfesion.DataSource = ControladorProfesion.GetData();
            ddlProfesion.DataTextField = "Descripcion";
            ddlProfesion.DataValueField = "Id";
            ddlProfesion.DataBind();

            ddlRelacion.DataSource = ControladorRelacion.GetData();
            ddlRelacion.DataTextField = "Descripcion";
            ddlRelacion.DataValueField = "Id";
            ddlRelacion.DataBind();

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (!ValidarRegistro())
            {
                if (RegistrarUsuario() > 0)
                {
                    string[] Origen = (string[])Session["IdOrigen"];
                    Origen[0] = "RegistroUser";
                    Origen[1] = "Se ha registrado el Usuario de forma correcta, el administrador del sistema le notificará sobre su rol en el sistema!";
                    Session["Origen"] = Origen;
                    Response.Redirect("Default.aspx");

                }
                else
                {
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = "Ocurrió un error durante el registro, contacte al administrador del sistema!";
                }
            }
        }

        bool ValidarRegistro()
        {
            string Mensaje = "";
            bool MensajeError = false;
            if (txtContrasena.Text != txtRContrasena.Text)
            {
                Mensaje = Mensaje + "Las contraseñas no coinciden, favor verifiquelas!" + Environment.NewLine;
                MensajeError = true;
            }
            if (txtTelefonoFijo.Text == "" && txtTelefonoCelular.Text == "")
            {
                Mensaje = Mensaje + "Debe ingresar al menos un número de contacto!" + Environment.NewLine;
                MensajeError = true;
            }
            if (txtTelefonoFijo.Text.Length > 0 && txtTelefonoFijo.Text.Length < 7)
            {
                Mensaje = Mensaje + "El número de télefono fijo esta incompleto!" + Environment.NewLine;
                MensajeError = true;
            }
            if (txtTelefonoCelular.Text.Length > 0 && txtTelefonoCelular.Text.Length < 10)
            {
                Mensaje = Mensaje + "El número de télefono celular esta incompleto!" + Environment.NewLine;
                MensajeError = true;
            }

            if (MensajeError)
            {
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
            }
            return MensajeError;
        }

        protected int RegistrarUsuario()
        {
            int resultado;
            if (ControladorUsuarioS.GetData().Rows.Count == 0)
            {
                resultado = ControladorUsuarioS.Insertar(txtIdentificacion.Text.Trim(),
                Convert.ToInt32(ddlTipoDocumento.SelectedValue),
                txtNombres.Text.Trim(),
                txtApellidos.Text.Trim(),
                Convert.ToInt32(ddlProfesion.SelectedValue),
                Convert.ToInt32(ddlProfesion.SelectedValue),
                Convert.ToInt32(txtTelefonoFijo.Text.Trim()),
                Convert.ToInt64(txtTelefonoCelular.Text.Trim()),
                txtCorreo.Text.Trim(),
                txtUsuario.Text,
                txtContrasena.Text,
                4,
                DateTime.Now.Date,
                DateTime.Now.Date,
                txtIdentificacion.Text,
                txtIdentificacion.Text);

            }
            else
            {
                if (Session["Login"] != null)
                {

                    UsuariosSistema ConsultaUser = (UsuariosSistema)Session["Login"];
                    resultado = ControladorUsuarioS.Insertar(txtIdentificacion.Text.Trim(),
                    Convert.ToInt32(ddlTipoDocumento.SelectedValue),
                    txtNombres.Text.Trim(),
                    txtApellidos.Text.Trim(),
                    Convert.ToInt32(ddlProfesion.SelectedValue),
                    Convert.ToInt32(ddlProfesion.SelectedValue),
                    Convert.ToInt32(txtTelefonoFijo.Text.Trim()),
                    Convert.ToInt64(txtTelefonoCelular.Text.Trim()),
                    txtCorreo.Text.Trim(),
                    txtUsuario.Text,
                    txtContrasena.Text,
                    1,
                    DateTime.Now.Date,
                    DateTime.Now.Date,
                    ConsultaUser.Identificacion,
                    ConsultaUser.Identificacion);
                }
                else
                {
                    resultado = ControladorUsuarioS.Insertar(txtIdentificacion.Text.Trim(),
                    Convert.ToInt32(ddlTipoDocumento.SelectedValue),
                    txtNombres.Text.Trim(),
                    txtApellidos.Text.Trim(),
                    Convert.ToInt32(ddlProfesion.SelectedValue),
                    Convert.ToInt32(ddlProfesion.SelectedValue),
                    Convert.ToInt32(txtTelefonoFijo.Text.Trim()),
                    Convert.ToInt64(txtTelefonoCelular.Text.Trim()),
                    txtCorreo.Text.Trim(),
                    txtUsuario.Text,
                    txtContrasena.Text,
                    1,
                    DateTime.Now.Date,
                    DateTime.Now.Date,
                    txtIdentificacion.Text,
                    txtIdentificacion.Text);
                }
            }

            return resultado;
        }
    }
}