﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="U_Usuario.aspx.cs" Inherits="WebApplication1.Formulario_web15" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <link href="Content/assets/css/aristo-ui.css" rel="stylesheet" />
    <link href="Content/assets/css/materialize.min.css" rel="stylesheet" />
    <script src="Content/assets/css/jquery-2.1.1.min.js"></script>
    <script src="Content/assets/css/materialize.min.js"></script>    
    <link href="Content/assets/css/material-dashboard.min.css" rel="stylesheet" />
    <script>
    
        $(document).ready(function(){
    $('.tabs').tabs();
  });
    </script>
    <script type='text/JavaScript'>       

     function verocultar(cual) {
          var c=cual.nextSibling;
          if(c.style.display=='none') {
               c.style.display='block';
          } else {
               c.style.display='none';
          }
          return false;
        }

        function moverseA(idDelElemento) {
            document.getElementById(idDelElemento).click();
        }

        function muestra_oculta(id){
            if (document.getElementById) {                
            var el = document.getElementById(id); 
                el.style.display = (el.style.display == 'none') ? 'block' : 'none'; 
                 
            }
        } 
        
       
</script>



        <%--Notificaciones--%>
    <div id="pnlNotificaciones" runat="server" visible="false" >
        <div runat="server" id="pnlContenedorNotificaciones" class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong><asp:Label ID="lblTituloMensaje" runat="server">Error</asp:Label></strong> <asp:Label runat="server" Text="" Id="lblMensaje"></asp:Label>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>

 

<%--    Tarjeta de registro de Usuario--%>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
               <div class="col-md-12" runat="server" id="plnComplementarios" Enabled="false">
                   <div class="card">
                      <div class="row">
                          <h3 style="margin-left:30px">Paciente: <asp:Label ID="lblpaciente" runat="server" Text="Consulte Un paciente!"></asp:Label>
                                                                  
                          </h3>
                      </div>
                      <div class="card-header card-header-tabs card-header-success">
                          <%-----------------------------Cabecera de Tabs---------------------------%>

                          <div class="nav-tabs-navigation">
                              <div class="nav-tabs-wrapper">
                                  <span class="nav-tabs-title"></span>
                                  <ul class="nav nav-tabs" data-tabs="tabs">
                                      <li class="nav-item">
                                          <a class="nav-link active" href="#Paciente" data-toggle="tab">
                                              <i class="material-icons">contacts</i> Paciente                           
                                              <div class="ripple-container"></div>
                                          </a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link" href="#Aspectos" data-toggle="tab">
                                              <i class="material-icons">code</i> Aspectos                           
                                              <div class="ripple-container"></div>
                                          </a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link" href="#Evaluacion" data-toggle="tab">
                                              <i class="material-icons">cloud</i> Evaluación y Plan de Intervención                           
                                              <div class="ripple-container"></div>
                                          </a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link" href="#Citas" data-toggle="tab">
                                              <i class="material-icons">alarm</i> Solicitud y Consulta de Citas                          
                                              <div class="ripple-container"></div>
                                          </a>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                          <%---------------------------Fin Cabecera de Tabs---------------------------%>
                      </div>
                      <div class="card-body">
                  <div class="tab-content" runat="server" id="pnlGeneral">
                    <div class="tab-pane active" id="Paciente">
                        <br style ="margin-top:-10px" />
                        <div style ="margin-top:-10px">                            
                                <a onclick="return verocultar(this);" href="javascript:void(0);" class="alert alert-warning col col-md-12" style="text-align:center;height:30px;"><h4><b>INFORMACIÓN DEL PACIENTE</b></h4></a><div id="PacienteDIV" style="display: none;">
                                   <br />
                                     <%---------------------------DatosPaciente---------------------------%>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table">
                                                <tr>
                                                    <td><asp:Label ID="lblNombreCompleto" runat="server" Text="Paciente:"></asp:Label></td>
                                                    <td><asp:Label ID="lblNombreCompletoValor" runat="server" Text=""></asp:Label></td>                                                                                                  
                                                </tr>
                                                <tr>
                                                    <td><asp:Label ID="lblIdentificacion" runat="server" Text="Identificacion:"></asp:Label></td>
                                                    <td><asp:Label ID="lblIdentificacionValor" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label ID="Label3" runat="server" Text="Identificacion:"></asp:Label></td>
                                                    <td><asp:Label ID="lblTipoIdentificacion" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label ID="Label1" runat="server" Text="Fecha de Nacimiento:"></asp:Label></td>
                                                    <td><asp:Label ID="lblFechaNacimiento" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label ID="Label2" runat="server" Text="Sexo:"></asp:Label></td>
                                                    <td><asp:Label ID="lblSexo" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                            </table>                                            
                                         </div>
                                        <div class="col-md-6">
                                            <table class="table">
                                                <tr>
                                                    <td><asp:Label ID="Label4" runat="server"  Text="Direcciónes:"></asp:Label></td>
                                                    <td><asp:Label ID="lblDireccion" runat="server" Text=""></asp:Label></td>                                                                                                  
                                                </tr>
                                                <tr>
                                                    <td><asp:Label ID="Label6" runat="server" Text="Barrio:"></asp:Label></td>
                                                    <td><asp:Label ID="lblBarrio" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label ID="Label8" runat="server" Text="Municipio:"></asp:Label></td>
                                                    <td><asp:Label ID="lblMunicipio" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label ID="Label10" runat="server" Text="Departamento:"></asp:Label></td>
                                                    <td><asp:Label ID="lblDepartamentos" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label ID="Label12" runat="server" Text="Telefonos:"></asp:Label></td>
                                                    <td><asp:Label ID="lblTelefonos" runat="server" Text=""></asp:Label></td>
                                                </tr>
                                                
                                            </table>                                            
                                         </div>

                                        </div>
                                    <div class="row" style="text-align:center">
                                        <asp:Button runat="server" ID="btnActualizarInfo"  
                                                        CssClass="btn btn-success" OnClick="btnActualizarInfo_Click" Text="Modificar"></asp:Button>                                       
                                      </div>
                                    </div>
                                    
                                </div> 
                            
                        <br />
                        <hr />
                        <br />
                         <div style ="margin-top:-10px">                            
                                <a onclick="return verocultar(this);" href="javascript:void(0);" class="alert alert-warning col col-md-12" style="text-align:center;height:30px;"><h4><b>INFORMACIÓN GENERAL - SEGURIDAD SOCIAL</b></h4></a><div style="display: none;">
                                   <br />
                                     <%---------------------------Información General---------------------------%>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table">
                                                <tr>
                                                    <td><asp:Label runat="server" Text="Ocupacion:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblOcupacion" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label runat="server" Text="Nivel Académico:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblNivelAcademico" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label runat="server" Text="Diagnóstico Según Usuario:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblDx" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label runat="server" Text="Tipo de Discapacidad:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblDiscapacidad" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label runat="server" Text="Remitente"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblRemitente" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label runat="server" Text="Religión"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblReligion" Text=""></asp:Label></td>
                                                </tr>
                                            </table>
                                        </div>
                                         <%---------------------------Seguridad Social---------------------------%>
                                        <div class="col-md-6">
                                            <table class="table">
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Label runat="server" Text="INFORMACIÓN DE SEGURIDAD SOCIAL"
                                                            class="alert alert-primary">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" Text="Tipo de Afiliación:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblTipoAfiliacion" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label runat="server" Text="EPS:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblEPS" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label runat="server" Text="Caja de Compensación:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblCaja" Text=""></asp:Label></td>
                                                </tr>
                                                 <tr>
                                                    <td><asp:Label runat="server" Text="Fondo de Pensiones:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblFondoPensiones" Text=""></asp:Label></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <asp:Button runat="server" ID="btnActualizarInfo2"  
                                                        CssClass="btn btn-success" OnClick="btnActualizarInfo_Click" Text="Modificar"></asp:Button>                                       
                                      </div>
                                </div> 
                            </div>
                        <br />
                        <hr />
                        <br />
                        <div style ="margin-top:-10px">                            
                                <a onclick="return verocultar(this);" href="javascript:void(0);" class="alert alert-warning col col-md-12" style="text-align:center;height:30px;"><h4><b>FAMILIARES DEL PACIENTE</b></h4></a><div style="display: none;">
                                   <br />
                                     <%---------------------------DatosPaciente---------------------------%>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        <asp:GridView runat="server" ID="grvFamiliares" class="table table-full-width table-bordered" AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:BoundField DataField="IdentificacionA" HeaderText="Identificacion"  />
                                                                <asp:BoundField DataField="Nombres" HeaderText="Nombres" />
                                                                <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" />
                                                                <asp:BoundField DataField="Sexo" HeaderText="Sexo" />
                                                                <asp:BoundField DataField="ParentescoDesc" HeaderText="Parentesco" />
                                                                <asp:BoundField DataField="Acudiente" HeaderText="Acudiente" />
                                                                <asp:BoundField DataField="Telefono1" HeaderText ="Teléfono" />
                                                                <asp:BoundField DataField="DireccionRes" HeaderText="Dirección" />
                                                                <asp:BoundField DataField="BarrioRes" HeaderText="Barrio" />
                                                            </Columns>
                                                            <HeaderStyle CssClass="thead-dark" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" style="text-align:center">
                                        <asp:Button runat="server" ID="btnModificarFamilia"  
                                                        CssClass="btn btn-success" OnClick="btnActualizarInfo_Click" Text="Modificar"></asp:Button>                                       
                                      </div>
                                </div> 
                            </div>
                        <br />
                        <hr />
                        <br />
                        <div style ="margin-top:-10px">                            
                                <a onclick="return verocultar(this);" href="javascript:void(0);" class="alert alert-warning col col-md-12" style="text-align:center;height:30px;"><h4><b>INFORMACIÓN FINANCIERA</b></h4></a><div style="display: none;">
                                   <br />
                                     <%---------------------------Informaciòn Financiera---------------------------%>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table">
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label5" Text="Depende Económicamente de:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblDepencencia" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label7" Text="Personas a Cargo:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblPersonasCargo" Text=""></asp:Label></td>
                                                
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label11" Text="Percibe Ingresos:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblPercibe" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label14" Text="Valor de Ingresos:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblIngresos" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label16" Text="Subsidios Económicos:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblPercibeSubsidios" Text=""></asp:Label></td>
                                                </tr>
                                                </table>
                                            </div>
                                        <div class="col-md-6">
                                                <table class="table">
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label18" Text="Valor de Subsidios:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblSubsidios" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label9" Text="Realiza Actividad Económica:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblRealizaActividad" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label15" Text="Cúal Actividad Económica:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblCualActividad" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label13" Text="Posee Vivienda Propia:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblViviendaPropia" Text=""></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label runat="server" ID="Label17" Text="Tipo de Vivienda:"></asp:Label></td>
                                                    <td><asp:Label runat="server" ID="lblTipoVivienda" Text=""></asp:Label></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row" style="text-align:center">
                                        <asp:Button runat="server" ID="btnActualizar3"  
                                                        CssClass="btn btn-success" OnClick="btnActualizarInfo_Click" Text="Modificar"></asp:Button>                                       
                                      </div>
                                </div> 
                            </div>
                        <br />
                        <hr />
                        <br />
                        
                        
                    </div>

                    <div class="tab-pane" id="Aspectos">
                      <div class="row">
                          <div class="col-md-2">
                            <div class="alert alert-info" style="margin-top:10px">
                                <a class="nav-link Active" href="#Medico" data-toggle="tab" ><b>Medico</b><div class="ripple-container" ></div></a>
                            </div>
                              <div class="alert alert-info" style="margin-top:5px">
                                <a class="nav-link " href="#Psicologico" data-toggle="tab"><b>Psicológico</b><div class="ripple-container" ></div></a>
                            </div>
                              <div class="alert alert-info" style="margin-top:5px">
                                <a class="nav-link" href="#Laboral" data-toggle="tab"><b>Laboral</b><div class="ripple-container" ></div></a>
                            </div>
                              <div class="alert alert-info" style="margin-top:5px">
                                <a class="nav-link" href="#VidaDiaria"  data-toggle="tab"><b>Vida Diaria</b><div class="ripple-container" ></div></a>
                            </div>
                              
                          </div>
                          <div class="col-md-9" id="Contenedores" >
                              <div class="tab-pane card card-content" id="Medico" style="margin-top:-10px">
                                  <asp:Label Text="Aspecto Médico del Paciente" runat="server"></asp:Label><hr />
                                  <div class="row">
                                      <table>
                                          <tr>
                                              <td width="150px">Diagnóstico Médico:</td>
                                              <td><asp:TextBox runat="server" ID="txtDxMedico"></asp:TextBox></td>
                                          </tr>
                                          <tr>
                                              <td width="150px">Tiempo en Discapacidad:</td>
                                              <td>Días:<asp:TextBox Width="70px" runat="server" ID="txtDiasD" Text="0" onkeyUp="return ValNumero(this);"></asp:TextBox>
                                                  Meses:<asp:TextBox Width="70px" runat="server" ID="txtMesesD" Text="0" onkeyUp="return ValNumero(this);"></asp:TextBox>
                                                  Años:<asp:TextBox Width="70px" runat="server" ID="txtAnosD" Text="0" onkeyUp="return ValNumero(this);"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="150px">Antecedentes Médicos Familiares:</td>
                                              <td><asp:TextBox runat="server" ID="txtAntecedentesM" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox></td>
                                          </tr>
                                          <tr>
                                              <td width="150px">Recomendaciones Médicas:</td>
                                              <td><asp:TextBox runat="server" ID="txtRecomendacionesM" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox></td>
                                          </tr>
                                          <tr>
                                              <td colspan="2" style="text-align:center">
                                                  
                                              </td>
                                          </tr>
                                      </table>
                                  </div>
                              </div>
                              <div class="tab-pane card card-content" id="Psicologico">
                                    <asp:Label Text="Aspecto Psicológico del Paciente" runat="server"></asp:Label><hr />
                                  <div class="row">
                                      <table>
                                          <tr>
                                              <td width="150px">Descripción del Comportamiento (asociado al momento de la entrevista) :</td>
                                              <td><asp:TextBox runat="server" ID="txtComportamientoAS" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px;Height:200px"></asp:TextBox></td>
                                          </tr>
                                          <tr>
                                              <td width="150px">Descripción del Estado Emocional:</td>
                                              <td><asp:TextBox runat="server" ID="txtEstadoEmocional" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px; Height:200px"></asp:TextBox></td>
                                          </tr>
                                          <tr>
                                              <td colspan="2" style="text-align:center"></td>
                                          </tr>
                                      </table>
                                  </div>
                              </div>
                              <div class="tab-pane card card-content" id="Laboral">
                                  <asp:Label Text="Aspecto Laboral del Paciente" runat="server"></asp:Label><hr />
                                  <div class="row">
                                      <table>
                                          <tr>
                                              <td>Tareas Desempeñadas:</td>
                                              <td><asp:TextBox runat="server" ID="txtTareasAL" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px;"></asp:TextBox></td>
                                          </tr>
                                          <tr>
                                              <td style="vertical-align:top">Intereses y Expectativas Laborales:</td>
                                              <td><asp:TextBox runat="server" ID="txtInteresesAL" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px; height:100px"></asp:TextBox></td>
                                          </tr>
                                           <tr>
                                              <td>Independencias Básicas:</td>
                                              <td>
                                                  <div class="row">
                                                      <div class="col-md-6">
                                                      <asp:CheckBox runat="server" Text="alimentación"></asp:CheckBox><br />
                                                      <asp:CheckBox runat="server" Text="Higiene Mayor(Bañarse)"></asp:CheckBox><br />
                                                      <asp:CheckBox runat="server" Text="Higiene Menor: limpieza bucal "></asp:CheckBox><br />
                                                          <asp:CheckBox runat="server" Text="Transporte "></asp:CheckBox><br />
                                                          <asp:CheckBox runat="server" Text="Transferencia Silla de ruedas"></asp:CheckBox><br />
                                                          <asp:CheckBox runat="server" Text="Otros aparatos ortopédicos"></asp:CheckBox><br />
                                                          </div>
                                                      <div class="col-md-6">
                                                      <asp:CheckBox runat="server" Text="Vestido  "></asp:CheckBox><br />
                                                      <asp:CheckBox runat="server" Text="Manejo de dinero "></asp:CheckBox><br />
                                                      <asp:CheckBox runat="server" Text="Actividades hogareñas "></asp:CheckBox><br />
                                                      <asp:CheckBox runat="server" Text="Uso Teléfono "></asp:CheckBox><br />
                                                     <asp:CheckBox runat="server" Text="Manejo Silla de Ruedas"></asp:CheckBox><br />                                                      
                                                      </div></div>
                                              </td>
                                          </tr>
                                           <tr>
                                               <td colspan="2" style="text-align:center">
                                                  </td>
                                           </tr>
                                      </table>
                                  </div>

                              </div>
                              <div class="tab-pane card card-content" id="VidaDiaria">
                                  <asp:Label Text="Aspectos de la Vida Cotidiana del Paciente" runat="server"></asp:Label><hr />
                                  <div class="row">
                                      <table>
                                          <tr>
                                              <td width="150px">Rutina Diaria:</td>
                                              <td style="vertical-align:middle">Hora:<asp:TextBox runat="server" type="Time" ID="txtHoraRD" Width="80px" style="margin-left:15px; border:1px solid green; border-radius:3px 4px;"></asp:TextBox>
                                                  Actividad: <asp:TextBox runat="server" ID="txtActividadRD" TextMode="MultiLine" Width="300px" style="height:200px; margin-left:15px; border:1px solid green; border-radius:3px 4px;"></asp:TextBox>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td colspan="2" style="text-align:center"><asp:Button ID="btnAgregarRD" runat="server" CssClass="btn btn-white" Text="Agregar" /></td>
                                              <td>
                                                  <asp:GridView ID="grvRutina" runat="server" CssClass="table" AutoGenerateColumns="false">
                                                      <Columns>
                                                          <asp:BoundField DataField="Hora" HeaderText="Hora" />
                                                           <asp:BoundField DataField="Actividad" HeaderText="Actividad" />
                                                      </Columns>
                                                      <HeaderStyle CssClass="thead-dark"/>
                                                  </asp:GridView>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="150px">Pasatiempos:</td>
                                              <td><asp:TextBox runat="server" ID="txtPasatiemposRD" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px; Height:200px"></asp:TextBox></td>
                                          </tr>
                                          <tr>
                                              <td colspan="2" style="text-align:center">                                                  
                                                  <asp:Button ID="btnRegistrar" runat="server" CssClass="btn btn-info" Text="Registrar" />
                                                  <asp:Button ID="btCancelar" runat="server" CssClass="btn btn-warning" Text="Cancelar"/>                                              
                                              </td>
                                          </tr>
                                      </table>
                                  </div>

                              </div>
                          </div>
                      </div>
                      </div>
                    
                    <div class="tab-pane" id="Evaluacion">
                      <div class="row">
                          <div class="col-md-5">
                              <div class="card">                                                                
                                  <div class="card-header card-header-info">
                                      <h5 class="card-title">Evaluación</h5>
                                    </div>
                                    <div class="card-body">
                                       <table>
                                           <tr>
                                               <td>Fecha de la Evaluación:</td>
                                               <td> 
                                                   <asp:TextBox ID="dtpFechaEvaluacion" TextMode="Date" class="form-control" runat="server"></asp:TextBox>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>Observaciones:</td>
                                               <td><asp:TextBox runat="server" ID="txtObservaciones" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox></td>
                                          </tr>
                                           <tr>
                                               <td>Conclusiones:</td>
                                               <td><asp:TextBox runat="server" ID="txtConclusiones" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox></td>
                                          </tr>
                                           <tr>
                                               <td>Evaluador:</td>
                                               <td>
                                                   <asp:DropDownList runat="server" ID="ddlEvaluador" class="form-control">
                                                   <asp:ListItem Value="" Text=""></asp:ListItem>
                                                   </asp:DropDownList>
                                               </td>
                                          </tr>
                                           <tr>
                                               <td colspan="2" style="text-align:center">
                                                   <asp:Button ID="btnRegistrarEV" runat="server" CssClass="btn btn-info" Text="Registrar" />
                                                  <asp:Button ID="btnCancelarEV" runat="server" CssClass="btn btn-warning" Text="Cancelar"/>
                                               </td>
                                           </tr>
                                       </table>
                                        </div>
                              
                                  </div>

                          </div>
                          <div class="col-md-7">
                               <div class="card">                                                                
                                  <div class="card-header card-header-warning">
                                      <h5 class="card-title">Plan de Intervención</h5>
                                    </div>
                                    <div class="card-body">
                                       <table>
                                           <tr>
                                               <td>Área o Servicio</td>
                                               <td> 
                                                   <asp:DropDownList runat="server" ID="ddlArea" class="form-control">
                                                       <asp:ListItem Value="Ninguno" Text="Ninguno"></asp:ListItem>
                                                       <asp:ListItem Value="Fisioterapia" Text="Fisioterapia"></asp:ListItem>
                                                       <asp:ListItem Value="Psicología:" Text="Psicología:"></asp:ListItem>
                                                       <asp:ListItem Value="Terapia Familiar" Text="Terapia Familiar"></asp:ListItem>
                                                       <asp:ListItem Value="Educación Especial" Text="Educación Especial"></asp:ListItem>
                                                       <asp:ListItem Value="Deporte Adaptado" Text="Deporte Adaptado"></asp:ListItem>
                                                   </asp:DropDownList>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>Metodología:</td>
                                               <td><asp:TextBox runat="server" ID="txtMetodología" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox></td>
                                          </tr>
                                           <tr>
                                               <td>Métodos:</td>
                                               <td><asp:TextBox runat="server" ID="txtMetodos" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox></td>
                                          </tr>
                                           <tr>
                                               <td>Actividades Propuestas:</td>
                                               <td><asp:TextBox runat="server" ID="txtActividades" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox></td>
                                          </tr>
                                           <tr>
                                               <td>Objetivos Generales:</td>
                                               <td><asp:TextBox runat="server" ID="txtObjetivosG" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox></td>
                                          </tr>
                                           <tr>
                                               <td>Objetivos Especificos:</td>
                                               <td><asp:TextBox runat="server" ID="txtObjetivosE" TextMode="MultiLine" style="border: 1px solid gray;border-radius:3px 4px"></asp:TextBox>                                                  
                                  
                                               </td>
                                          </tr>
                                           <tr>
                                               <td colspan="2" style="text-align:center">
                                                   <asp:LinkButton runat="server" ID="lnkAgregarPA" ValidationGroup="ValRegistroUsuario3" CssClass="btn btn-info" Text="Agregar"></asp:LinkButton>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td colspan="2" style="text-align:center">PLANES DE INTERVENCIÓN:</td>                                               
                                          </tr>
                                           <tr>
                                               <td colspan="2">
                                                   <asp:GridView runat="server"></asp:GridView>
                                               </td>                                               
                                          </tr>
                                           
                                           <tr>
                                               <td colspan="2" style="text-align:center">
                                                   <asp:Button ID="btnRegistrarPI" runat="server" CssClass="btn btn-info" Text="Registrar" />
                                                  <asp:Button ID="btnCancelarPI" runat="server" CssClass="btn btn-warning" Text="Cancelar"/>
                                               </td>
                                           </tr>
                                       </table>
                                        </div>
                              
                                  </div>

                          </div>
                      </div>
                    </div>

                    <div class="tab-pane" id="Citas">
                        <div class="row">
                          <div class="col-md-5">
                              <div class="card">                                                                
                                  <div class="card-header card-header-info">
                                      <h5 class="card-title">Solicitud de Citas</h5>
                                    </div>
                                    <div class="card-body">
                                       <table>
                                           <tr>
                                               <td>Área o Servicio</td>
                                               <td> 
                                                  
                                                   <asp:DropDownList runat="server" ID="ddlCita" class="form-control">
                                                       <asp:ListItem Value="" Text=""></asp:ListItem>
                                                       <asp:ListItem Value="Fisioterapia" Text="Fisioterapia"></asp:ListItem>
                                                       <asp:ListItem Value="Psicología:" Text="Psicología:"></asp:ListItem>
                                                       <asp:ListItem Value="Terapia Familiar" Text="Terapia Familiar"></asp:ListItem>
                                                       <asp:ListItem Value="Educación Especial" Text="Educación Especial"></asp:ListItem>
                                                       <asp:ListItem Value="Deporte Adaptado" Text="Deporte Adaptado"></asp:ListItem>
                                                   </asp:DropDownList>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>Fecha Sugerida de la Cita:</td>
                                               <td> 
                                                   <asp:TextBox ID="dtpFechaCita" TextMode="Date" class="form-control" runat="server"></asp:TextBox>
                                               </td>
                                           </tr>
                                            <tr>
                                               <td>Hora Sugerida de la Cita:</td>
                                               <td> 
                                                   <asp:TextBox ID="txtHora" TextMode="time" class="form-control" runat="server"></asp:TextBox>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td>Profesional:</td>
                                               <td> <asp:DropDownList runat="server" ID="ddlProfesional" class="form-control">
                                                   <asp:ListItem Value="" Text=""></asp:ListItem>
                                                   </asp:DropDownList></td>
                                          </tr>
                                           
                                           <tr>
                                               <td colspan="2" style="text-align:center">
                                                   <asp:Button ID="btnRegistrarCita" runat="server" CssClass="btn btn-info" Text="Asignar Cita" />
                                                  <asp:Button ID="btnCancelarCita" runat="server" CssClass="btn btn-warning" Text="Cancelar"/>
                                               </td>
                                           </tr>
                                       </table>
                                        </div>
                              
                                  </div>

                          </div>
                          <div class="col-md-7">
                               <div class="card">                                                                
                                  <div class="card-header card-header-warning">
                                      <h5 class="card-title">Citas Solicitadas</h5>
                                    </div>
                                    <div class="card-body">
                                       <table>
                                          <tr>
                                              <td>
                                                   <asp:Label runat="server" Text="Sin citas asignadas" ID="lblcitas"> </asp:Label>
                                                <asp:GridView runat="server" ID="grvCita" class="table table-full-width table-bordered" AutoGenerateColumns="false">
                                                            <Columns>
                                                                <asp:BoundField DataField="Fecha" HeaderText="Fecha"  />
                                                                <asp:BoundField DataField="Hora" HeaderText="Hora" />
                                                                <asp:BoundField DataField="Profesional" HeaderText="Profesional" />
                                                                <asp:BoundField DataField="Servicio" HeaderText="Servicio" />
                                                                <asp:BoundField DataField="Lugar" HeaderText="Lugar" />
                                                                 <asp:CommandField SelectText="Eliminar" ButtonType="Image" SelectImageUrl="~/Content/Iconos/error.png" ShowSelectButton="True">
                                                                  <ItemStyle Width="50px" /> <HeaderStyle Width="50px" />
                                                                  </asp:CommandField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="thead-dark" />
                                                        </asp:GridView>  
                                              </td>
                                          </tr>
                                       </table>
                                        </div>
                              
                                  </div>

                          </div>
                      </div>
                    </div>
                   </div>
                   </div>
                  </div>
                </div>
              </div>
              </div>
                     
                  </div>
 <script language="javascript" type="text/javascript">

        function Solo_Numerico(variable) {
            Numer = parseInt(variable);
            if (isNaN(Numer)) {
                return "";
            }
            return Numer;
        }
        function ValNumero(Control) {
            Control.value = Solo_Numerico(Control.value);
        }
    </script>
</asp:Content>
