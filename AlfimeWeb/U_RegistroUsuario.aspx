﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="U_RegistroUsuario.aspx.cs" Inherits="WebApplication1.Formulario_web12" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
<link href="Content/assets/css/aristo-ui.css" rel="stylesheet" />
    <%--<link href="Content/assets/css/materialize.min.css" rel="stylesheet" />--%>
    <script src="Content/assets/css/jquery-2.1.1.min.js"></script>
    <script src="Content/assets/css/materialize.min.js"></script>    <script src="js/bootstrap.min.js"></script>
    <script src="Content/assets/js/core/bootstrap-material-design.min.js"></script>
   <%-- <link href="Content/assets/css/material-dashboard.min.css" rel="stylesheet" />--%>
    <script id="Seleccion_Sexo">   
        
       function seleccionar_Hombre()
       {
           var Hombre = document.getElementById("MainContent_chbHombre").checked;
           var Mujer = document.getElementById("MainContent_chbMujer").checked;
           if (Hombre == true)
           {
               document.getElementById("MainContent_chbHombre").checked = true;
               document.getElementById("MainContent_chbMujer").checked = false;
           }
        } 

       function seleccionar_Mujer()
       { 
           var Hombre = document.getElementById("MainContent_chbHombre").checked;
           var Mujer = document.getElementById("MainContent_chbMujer").checked;
           if (Mujer==true)
           {
               document.getElementById("MainContent_chbHombre").checked = false;
               document.getElementById("MainContent_chbMujer").checked = true;
           }
        } 

        function moverseA(idDelElemento) {
            document.getElementById(idDelElemento).click();
        }

    </script>

    <%--Notificaciones--%>
    <div id="pnlNotificaciones" runat="server" visible="false" >
        <div runat="server" id="pnlContenedorNotificaciones" class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong><asp:Label ID="lblTituloMensaje" runat="server">Error</asp:Label></strong> <asp:Label runat="server" Text="" Id="lblMensaje"></asp:Label>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
 <%--Notificaciones--%>


<%--    Tarjeta de registro de Usuario--%>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-5">
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title">Nuevo Usuario</h4>
                  <p class="card-category">Por favor realice completamente el registro del usuario</p>
                </div>
                <div class="card-body">  
                    
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Identificación</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" style="padding-left:-10em"
                                ControlToValidate="txtIdentificacion"
                                ErrorMessage="El Campo es Obligatorio"
                                ForeColor="Red" Display="Dynamic" 
                                ToolTip="El Campo es Obligatorio" 
                                ValidationGroup="ValRegistroUsuario"><img src="Content/Iconos/error.png"/></asp:RequiredFieldValidator>
                          <asp:TextBox ID="txtIdentificacion" autopostback="true" class="form-control" runat="server" OnTextChanged="txtIdentificacion_TextChanged"></asp:TextBox>                          
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Tipo de documento</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" style="padding-left:-10em"
                                ControlToValidate="ddlTipoDocumento"
                                ErrorMessage="El Campo es Obligatorio"
                                ForeColor="Red" Display="Dynamic" 
                                ToolTip="El Campo es Obligatorio" 
                                ValidationGroup="ValRegistroUsuario"><img src="Content/Iconos/error.png"/></asp:RequiredFieldValidator>
                          <asp:DropDownList ID="ddlTipoDocumento" class="form-control" runat="server"></asp:DropDownList>                          
                        </div>
                      </div>    
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Primer Nombre</label>
                            <asp:RequiredFieldValidator ID="miValidador" runat="server" style="padding-left:-10em"
                                ControlToValidate="txtNombre1"
                                ErrorMessage="El Campo es Obligatorio"
                                ForeColor="Red" Display="Dynamic" 
                                ToolTip="El Campo es Obligatorio" 
                                ValidationGroup="ValRegistroUsuario"><img src="Content/Iconos/error.png"/></asp:RequiredFieldValidator>
                          <asp:TextBox ID="txtNombre1" class="form-control" runat="server"></asp:TextBox>   
                            
                        </div>
                      </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Segundo Nombre</label>
                          <asp:TextBox ID="txtNombre2" class="form-control" runat="server"></asp:TextBox>                          
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Primer Apellido</label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" style="padding-left:-10em"
                                ControlToValidate="txtApellido1"
                                ErrorMessage="El Campo es Obligatorio"
                                ForeColor="Red" Display="Dynamic" 
                                ToolTip="El Campo es Obligatorio" 
                                ValidationGroup="ValRegistroUsuario"><img src="Content/Iconos/error.png"/></asp:RequiredFieldValidator>
                          <asp:TextBox ID="txtApellido1" class="form-control" runat="server"></asp:TextBox>                          
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Segundo Apellido</label>
                          <asp:TextBox ID="txtApellido2" class="form-control" runat="server"></asp:TextBox>                          
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                          <table>
                              <tr>
                                   <td>
                                       <label class="bmd-label-floating">Fecha de Nacimiento</label>
                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" style="padding-left:-10em"
                                ControlToValidate="dtpFechaNacimiento"
                                ErrorMessage="El Campo es Obligatorio"
                                ForeColor="Red" Display="Dynamic" 
                                ToolTip="El Campo es Obligatorio" 
                                ValidationGroup="ValRegistroUsuario"><img src="Content/Iconos/error.png"/></asp:RequiredFieldValidator>
                                   </td>
                              </tr>
                              <tr>
                                  <td>
                                      <asp:TextBox ID="dtpFechaNacimiento" TextMode="Date" class="form-control" runat="server"></asp:TextBox>
                                  </td>
                              </tr>
                          </table>
                      
                      </div>
                    
                      <div class="col-md-6">
                          <table >
                              <tr>
                                  <td class="auto-style3" colspan="2">Sexo: </td>
                              </tr>
                              <tr>                                   
                                  <td>
                                      <div class="form-check">
                                        <label class="form-check-label">
                                          <input class="form-check-input" id="chbHombre" type="checkbox" value=""
                                              runat="server" onclick="seleccionar_Hombre()" checked="checked">
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>Hombre
                                        </label>
                                    </div>
                                </td>                                  
                                
                                  <td class="auto-style3"></td>
                                  <td>
                                      <div class="form-check" style="vertical-align:middle;">
                                        <label class="form-check-label">
                                          <input class="form-check-input" type="checkbox" id="chbMujer" value="" runat="server" 
                                             onclick="seleccionar_Mujer()"
                                              >
                                          <span class="form-check-sign">
                                            <span class="check"></span>
                                          </span>Mujer
                                        </label>
                                    </div>
                                </td>
                                  <td class="auto-style3">
                                    
                                  </td>                               
                              </tr>
                              
                          </table>
                       
                      </div>
                        
                    </div>
                    <div class="row">

                        <div class="col-md-12" style="text-align: center">
                            <asp:Button runat="server" ID="btnCrearPaciente" OnClick="btnCrearPaciente_Click"
                               validationGroup="ValRegistroUsuario" CssClass="btn btn-success" Text="Crear Usuario"></asp:Button>
                        </div>
                        <div class="col-md-0" style="text-align: left">
                        </div>
                    </div>
                  </div>
                </div>
                  </div>
                

              <div class="col-md-7" runat="server" id="plnComplementarios" Enabled="false">
                  <div class="card">
                      <div class="card-header card-header-tabs card-header-primary">
                          <%-----------------------------Cabecera de Tabs---------------------------%>

                          <div class="nav-tabs-navigation">
                              <div class="nav-tabs-wrapper">
                                 
                                  <ul class="nav nav-tabs" data-tabs="tabs">
                                      <li class="nav-item">
                                          <a class="nav-link active" href="#InfoAdicional" data-toggle="tab" style="text-align:center">
                                              Información<br />Adicional                           
                                              <div class="ripple-container"></div>
                                          </a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link" href="#SeguridadSocial" data-toggle="tab" style="text-align:center" runat="server">
                                               Seguridad <br />Social                           
                                              <div class="ripple-container"></div>
                                          </a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link" href="#AcuFamiliares" id="idAcuFamiliares" data-toggle="tab" style="text-align:center">
                                               Acudientes  <br /> Familiares                           
                                              <div class="ripple-container"></div>
                                          </a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link" href="#InfoFinanciera" data-toggle="tab" style="text-align:center">
                                               Información<br />Financiera
                                              <div class="ripple-container"></div>
                                          </a>
                                      </li>
                                  </ul>
                              </div>
                          </div>
                          <%---------------------------Fin Cabecera de Tabs---------------------------%>
                      </div>
                      <div class="card-body">
                          <div class="tab-content">
                              <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                              <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                              <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                              <!--/////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                              <div class="tab-pane active" id="InfoAdicional">
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Teléfono:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtTelefono"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-4" >                                      
                                       <asp:TextBox ID="txtTelefono" class="form-control" onkeyUp="return ValNumero(this);" runat="server" style="padding-left:-10px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3    " style="padding-top:13px; text-align:right">Teléfono 2:                                       
                                    </div>
                                    <div class="col-md-4" >                                      
                                       <asp:TextBox ID="txtTelefono2" class="form-control" onkeyUp="return ValNumero(this);" runat="server" style="padding-left:-10px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Departamento:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlDepartamento"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-3" >                                      
                                       <asp:DropDownList ID="ddlDepartamento" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged" class="form-control"  runat="server" style="padding-left:-10px" AutoPostBack="True"></asp:DropDownList>
                                    </div>
                                       <div class="col-md-2" style="padding-top:13px; text-align:right">Ciudad:
                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlCiudad"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                       </div>
                                    <div class="col-md-4" >                                      
                                       <asp:DropDownList ID="ddlCiudad" class="form-control"  runat="server" style="padding-left:0px; text-align:left"></asp:DropDownList>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Barrio:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtBarrio"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-4" >                                      
                                       <asp:TextBox ID="txtBarrio" class="form-control"  runat="server" style="padding-left:-10px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Dirección:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtDireccion"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-9" >                                      
                                       <asp:TextBox ID="txtDireccion" class="form-control" runat="server" style="padding-left:-1px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Dirección 2:                                       
                                    </div>
                                    <div class="col-md-9" >                                      
                                       <asp:TextBox ID="txtDireccion2" class="form-control" runat="server" style="padding-left:-1px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                                  <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                                  <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-->
                                  <div class="row">                                  
                                    <div class="col-md-12" style="text-align:center; background-color:darkslateblue; color:white "><p><br/><strong>INFORMACIÓN ADICIONAL</strong></p></div> 
                                   
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px;text-align:right">Ocupación:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtOcupacion"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-8" >                                      
                                       <asp:TextBox ID="txtOcupacion" class="form-control" runat="server" style="padding-left:-1px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px;text-align:right">Nivel Académico:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlNivelAcademico"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-4" >                                      
                                       <asp:Dropdownlist ID="ddlNivelAcademico" class="form-control" runat="server" style="padding-left:-1px">                                           
                                       </asp:Dropdownlist>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px;text-align:right">Diagnóstico Usuario:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtDxUsuario"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-8" >                                      
                                       <asp:TextBox ID="txtDxUsuario" class="form-control" runat="server"  style="padding-left:-1px; height:80px" 
                                           TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px;text-align:right">Tipo Discapacidad:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlTipoDiscapacidad"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-8" >                                      
                                       <asp:Dropdownlist ID="ddlTipoDiscapacidad" class="form-control" runat="server"  style="padding-left:-1px;">
                                           <asp:ListItem Value="" Text=""></asp:ListItem>
                                           <asp:ListItem Value="Motriz" Text="Motriz"></asp:ListItem>
                                           <asp:ListItem Value="Sensorial" Text="Sensorial"></asp:ListItem>
                                           <asp:ListItem Value="Intelectual" Text="Intelectual"></asp:ListItem>
                                           <asp:ListItem Value="Mental" Text="Mental"></asp:ListItem>
                                           <asp:ListItem Value="Psíquica" Text="Psíquica"></asp:ListItem>
                                           <asp:ListItem Value="Visceral" Text="Visceral"></asp:ListItem>
                                           <asp:ListItem Value="Múltiple" Text="Múltiple"></asp:ListItem>
                                       </asp:Dropdownlist>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px;text-align:right">Remitente:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtRemitente"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-8" >                                      
                                       <asp:TextBox ID="txtRemitente" class="form-control" runat="server"  style="padding-left:-1px;">
                                       </asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px;text-align:right">Religión:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtReligion"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-8" >                                      
                                       <asp:TextBox ID="txtReligion" class="form-control" runat="server" style="padding-left:-1px"></asp:TextBox>
                                    </div>
                                  </div>

                                  <!--///////////////////////////////////////////////////////////////////////////////////////////-->
                                  <div class="row">
                                      <div class="col-md-12" style="text-align: center">
                                          <a href="#SeguridadSocial" class="btn btn-success" data-toggle="tab">Continuar</a>
                                      </div>
                                      <div class="col-md-0" style="text-align: left">
                                      </div>
                                  </div>

                              </div>
                    
                              <div class="tab-pane" id="SeguridadSocial">
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Tipo Afiliación:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="ddlTipoAfiliacion"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValSeguridadSocial"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-4">
                                          <asp:Dropdownlist ID="ddlTipoAfiliacion" class="form-control" runat="server" Style="padding-left: -10px">
                                              <asp:ListItem Value="" Text=""></asp:ListItem>
                                              <asp:ListItem Value="Sin afiliación" Text="Sin afiliación"></asp:ListItem>
                                              <asp:ListItem Value="SISBEN" Text="SISBEN"></asp:ListItem>
                                              <asp:ListItem Value="Subsidiada" Text="Subsidiada"></asp:ListItem>
                                              <asp:ListItem Value="Contributiva" Text="Contributiva"></asp:ListItem>
                                              <asp:ListItem Value="Prepagada" Text="Prepagada"></asp:ListItem>
                                              <asp:ListItem Value="Magisterio" Text="Magisterio"></asp:ListItem>
                                              <asp:ListItem Value="Especial" Text="Especial"></asp:ListItem>
                                          </asp:Dropdownlist>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          EPS:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="ddlEPS"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValSeguridadSocial"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-8">
                                          <asp:Dropdownlist ID="ddlEPS" class="form-control" runat="server" Style="padding-left: -10px">
                                          </asp:Dropdownlist>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Caja Compensación:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="ddlCajaCompensacion"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValSeguridadSocial"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-3">
                                          <asp:Dropdownlist ID="ddlCajaCompensacion" class="form-control" runat="server" Style="padding-left: -10px">
                                              <asp:ListItem Value="" Text=""></asp:ListItem>
                                              <asp:ListItem Value="Si" Text="Si"></asp:ListItem>
                                              <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                          </asp:Dropdownlist>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Cajas de Compensación:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="ddlCajaS_Compensacion"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValSeguridadSocial"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-6">
                                          <asp:Dropdownlist ID="ddlCajaS_Compensacion" class="form-control" runat="server" Style="padding-left: -10px">                                              
                                          </asp:Dropdownlist>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Fondo de Pensión:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="ddlFondosPension"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValSeguridadSocial"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-6">
                                          <asp:Dropdownlist ID="ddlFondosPension" class="form-control" runat="server" Style="padding-left: -10px">
                                              <asp:ListItem Value="" Text=""></asp:ListItem>
                                              <asp:ListItem Value="Si" Text="Si"></asp:ListItem>
                                              <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                          </asp:Dropdownlist>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Fondos de Pensión:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="ddlFondo_Pensiones"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValSeguridadSocial"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-8">
                                          <asp:Dropdownlist ID="ddlFondo_Pensiones" class="form-control" runat="server" Style="padding-left: -10px">                                              
                                          </asp:Dropdownlist>
                                      </div>
                                  </div>
                                   <div class="row">
                                      <div class="col-md-6" style="text-align: right">
                                          <a href="#InfoAdicional" class="btn btn-success" data-toggle="tab">Anterior</a>
                                      </div>
                                       <div class="col-md-6" style="text-align: left">
                                          <a href="#AcuFamiliares" class="btn btn-success" data-toggle="tab">Continuar</a>
                                      </div>
                                  </div>

                              </div>
                    
                              <div class="tab-pane" id="AcuFamiliares">
                                  <div class="row">                                  
                                    <div class="col-md-12" style="text-align:center; background-color:darkslateblue; color:white "><p><br/><strong>INFORMACIÓN FAMILIAR</strong></p></div>                                    
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px; text-align:right">Tipo de Identificación:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlTipoIdentificacion"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-4" >                                      
                                       <asp:Dropdownlist ID="ddlTipoIdentificacion" class="form-control" runat="server" style="padding-left:-10px"></asp:Dropdownlist>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px; text-align:right">Identificación:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtidenificacionF"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-5" >                                      
                                       <asp:TextBox ID="txtidenificacionF" class="form-control" runat="server" style="padding-left:-10px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px; text-align:right">Nombres:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtNombresF"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-8" >                                      
                                       <asp:TextBox ID="txtNombresF" class="form-control" runat="server" style="padding-left:-10px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px; text-align:right">Apellidos:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtApellidosF"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-8" >                                      
                                       <asp:TextBox ID="txtApellidosF" class="form-control" runat="server" style="padding-left:-10px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px; text-align:right">Edad:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtEdadF"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-2" >                                      
                                       <asp:TextBox ID="txtEdadF" class="form-control" runat="server"  onkeyUp="return ValNumero(this);" MaxLength="2" style="padding-left:-10px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px; text-align:right">Sexo:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlSexo"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-5" >                                      
                                       <asp:Dropdownlist ID="ddlSexo" class="form-control" runat="server" style="padding-left:-10px">
                                           <asp:ListItem Value="" Text=""></asp:ListItem>
                                           <asp:ListItem Value="Hombre" Text="Hombre"></asp:ListItem>
                                           <asp:ListItem Value="Mujer" Text="Mujer"></asp:ListItem>
                                       </asp:Dropdownlist>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px; text-align:right">Parentesco:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlParentesco"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-5" >                                      
                                       <asp:Dropdownlist ID="ddlParentesco" class="form-control" runat="server" style="padding-left:-10px">
                                           <asp:ListItem Value="" Text=""></asp:ListItem>
                                           
                                       </asp:Dropdownlist>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-4" style="padding-top:13px; text-align:right">Acudiente:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlAcudiente"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-5" >                                      
                                       <asp:Dropdownlist ID="ddlAcudiente" class="form-control" runat="server" style="padding-left:-10px">
                                           <asp:ListItem Value="NO" Text="NO"></asp:ListItem>
                                           <asp:ListItem Value="SI" Text="SI"></asp:ListItem>
                                       </asp:Dropdownlist>
                                    </div>
                                  </div>
                                  <br />
                                  <div class="row" style="height:1px; background-color:black">

                                      </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Departamento:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlDepartamentoF"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-3" >                                      
                                       <asp:DropDownList ID="ddlDepartamentoF" OnSelectedIndexChanged="ddlDepartamentoF_SelectedIndexChanged" AutoPostBack="true" class="form-control"  runat="server" style="padding-left:-10px"></asp:DropDownList>
                                    </div>
                                       <div class="col-md-2" style="padding-top:13px; text-align:right">Ciudad:
                                           <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" style="padding-left:-10em"
                                            ControlToValidate="ddlCiudadF"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                       </div>
                                    <div class="col-md-4" >                                      
                                       <asp:DropDownList ID="ddlCiudadF" class="form-control"  runat="server" style="padding-left:0px; text-align:left"></asp:DropDownList>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Barrio:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtBarrioF"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-4" >                                      
                                       <asp:TextBox ID="txtBarrioF" class="form-control"  runat="server" style="padding-left:-10px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Dirección:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtDireccionF"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-9" >                                      
                                       <asp:TextBox ID="txtDireccionF" class="form-control" runat="server" style="padding-left:-1px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px; text-align:right">Dirección 2:                                       
                                    </div>
                                    <div class="col-md-9" >                                      
                                       <asp:TextBox ID="txtDireccionF2" class="form-control" runat="server" style="padding-left:-1px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px;text-align:right">Teléfono:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" style="padding-left:-10em"
                                            ControlToValidate="txtTelefonoF1"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic" 
                                            ToolTip="El Campo es Obligatorio" 
                                            ValidationGroup="ValRegistroUsuario3"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-md-6" >                                      
                                       <asp:TextBox ID="txtTelefonoF1" class="form-control"  onkeyUp="return ValNumero(this);" runat="server" style="padding-left:-1px"></asp:TextBox>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-3" style="padding-top:13px;text-align:right">Teléfono 2:</div>
                                    <div class="col-md-6" >                                      
                                       <asp:TextBox ID="txtTelefonoF2" class="form-control"  onkeyUp="return ValNumero(this);" runat="server" style="padding-left:-1px"></asp:TextBox><br />
                                    </div>
                                  </div>
                                  <div class="row" style="margin-left:40%">
                                      <asp:LinkButton runat="server" ID="lnkAgregarFamiliar" ValidationGroup="ValRegistroUsuario3" OnClick="lnkAgregarFamiliar_Click" CssClass="btn btn-info" Text="Agregar"></asp:LinkButton>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-6" style="text-align: right">
                                          <a href="#SeguridadSocial" class="btn btn-success" data-toggle="tab">Anterior</a>
                                      </div>
                                       <div class="col-md-6" style="text-align: left">
                                          <a href="#InfoFinanciera" class="btn btn-success" data-toggle="tab">Continuar</a>
                                      </div>
                                  </div>
                                  <div class="row" style="height:1px; background-color:black"></div>
                                  <div class="row">
                                      <asp:GridView ID="grvFamiliares" runat="server" class="table"
                                          AutoGenerateColumns="False" OnRowCommand="grvFamiliares_RowCommand"
                                          OnSelectedIndexChanged="grvFamiliares_SelectedIndexChanged" DataKeyNames="IdentificacionA" 
                                          style="width:100%; word-wrap:break-word;table-layout:fixed;padding-top:4em">
                                          <Columns>
                                              <asp:BoundField AccessibleHeaderText="Identificacion"  DataField="IdentificacionA" HeaderText="Identificación" >
                                                  <ItemStyle Width="150px" /> <HeaderStyle Width="150px" />
                                              </asp:BoundField>
                                              <asp:BoundField AccessibleHeaderText="Nombres" DataField="Nombres" HeaderText="Nombres" />
                                              <asp:BoundField AccessibleHeaderText="Apellidos" DataField="Apellidos" HeaderText="Apellidos" />
                                              <asp:BoundField AccessibleHeaderText="Edad" DataField="Edad" HeaderText="Edad" />
                                              <asp:BoundField AccessibleHeaderText="Parentesco" DataField="Parentesco" HeaderText="Parentesco" />
                                              <asp:CommandField SelectText="Eliminar" ButtonType="Image" SelectImageUrl="~/Content/Iconos/error.png" ShowSelectButton="True">
                                                  <ItemStyle Width="50px" /> <HeaderStyle Width="50px" />
                                                  </asp:CommandField>
                                          </Columns>
                                          <HeaderStyle CssClass="thead-dark" />
                                      </asp:GridView>
                                      <asp:PlaceHolder ID = "PlaceHolder1" runat="server" />
                                   </div>                              
                              </div>

                              <div class="tab-pane" id="InfoFinanciera">
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Dependencia Financiera:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="ddlDependencia"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-4">
                                          <asp:Dropdownlist ID="ddlDependencia" class="form-control" runat="server" Style="padding-left: -10px">
                                              <asp:ListItem Value="Ninguna" Text="Ninguna"></asp:ListItem>
                                              <asp:ListItem Value="Familiar" Text="Familiar"></asp:ListItem>
                                              <asp:ListItem Value="Estatal" Text="Estatal"></asp:ListItem>
                                              <asp:ListItem Value="Organizaciones No Gubernamentales" Text="Organizaciones No Gubernamentales"></asp:ListItem>
                                              <asp:ListItem Value="Donaciones o Caridad" Text="Donaciones o Caridad"></asp:ListItem>
                                          </asp:Dropdownlist>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Nro Personas a Cargo:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="txtPersonasCargo"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-2">
                                          <asp:TextBox ID="txtPersonasCargo" onkeyUp="return ValNumero(this);" Text="0" MaxLength="2" class="form-control" runat="server" Style="padding-left: -10px">                                              
                                          </asp:TextBox>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Percibe Ingresos:
                                      </div>
                                      <div class="col-md-2">
                                          <div class="form-check">
                                              <label class="form-check-label">
                                                  <input class="form-check-input" id="chbPercibeIngresos" type="checkbox" value=""
                                                      runat="server">
                                                  <span class="form-check-sign">
                                                      <span class="check"></span>
                                                  </span>
                                              </label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Valor Ingresos Mensuales:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="txtVlrIngresos"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-8">
                                          <label>
                                          <asp:TextBox ID="txtVlrIngresos" onkeyUp="return ValNumero(this);" MaxLength="10" class="form-control" runat="server" Style="padding-left: -10px">0</asp:TextBox>Sin separadores de mil o decimales.</label>
                                      </div>
                                  </div>
                                  <!---->
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Subsidio Económico:
                                      </div>
                                      <div class="col-md-2">
                                          <div class="form-check">
                                              <label class="form-check-label">
                                                  <input class="form-check-input" id="chbSubsidio" type="checkbox" value=""
                                                      runat="server">
                                                  <span class="form-check-sign">
                                                      <span class="check"></span>
                                                  </span>
                                              </label>
                                          </div>
                                      </div>
                                  </div>
                                  <!---->
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Valor Subsidio:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="txtVlrSubsidio"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-8">
                                          <label>
                                          <asp:TextBox ID="txtVlrSubsidio" onkeyUp="return ValNumero(this);" MaxLength="10" class="form-control" runat="server" Style="padding-left: -10px">0</asp:TextBox>Sin separadores de mil o decimales.</label>
                                      </div>
                                  </div>
                                  <!---->
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Actividad Económica:
                                      </div>
                                      <div class="col-md-2">
                                          <div class="form-check">
                                              <label class="form-check-label">
                                                  <input class="form-check-input" id="chbActividadEco" type="checkbox" value=""
                                                      runat="server">
                                                  <span class="form-check-sign">
                                                      <span class="check"></span>
                                                  </span>
                                              </label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                         ¿Cúal actividad?:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="txtCualActividad"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-8">                                          
                                          <asp:TextBox ID="txtCualActividad" class="form-control" runat="server" Style="padding-left: -10px" Text="Ninguna"></asp:TextBox>
                                      </div>
                                  </div>
                                   <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                          Vivienda Propia:
                                      </div>
                                      <div class="col-md-2">
                                          <div class="form-check">
                                              <label class="form-check-label">
                                                  <input class="form-check-input" id="chbViviendaPropia" type="checkbox" value=""
                                                      runat="server">
                                                  <span class="form-check-sign">
                                                      <span class="check"></span>
                                                  </span>
                                              </label>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-md-4" style="padding-top: 13px; text-align: right">
                                         Tipo de Vivienda:
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" Style="padding-left: -10em"
                                            ControlToValidate="ddlTipoVivienda"
                                            ErrorMessage="El Campo es Obligatorio"
                                            ForeColor="Red" Display="Dynamic"
                                            ToolTip="El Campo es Obligatorio"
                                            ValidationGroup="ValRegistroUsuario2"><img src="Content/Iconos/error.png"/>
                                        </asp:RequiredFieldValidator>
                                      </div>
                                      <div class="col-md-8">                                          
                                          <asp:Dropdownlist ID="ddlTipoVivienda" class="form-control" runat="server" Style="padding-left: -10px"></asp:Dropdownlist>
                                      </div>
                                  </div>
                                  <br />
                                  <div class="row">
                                      <div class="col-md-12" style="text-align: center">
                                          <a href="#AcuFamiliares" class="btn btn-success" data-toggle="tab">Anterior</a>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-md-12" style="text-align: center">
                                          <asp:Button runat="server" ID="btnAgregarInfoAdicional"  
                                              CssClass="btn btn-success" Onclick="btnAgregarInfoAdicional_Click" Text="Agregar Información Adicional"></asp:Button>
                                      </div>
                                      <div class="col-md-0" style="text-align: left">
                                      </div>
                                  </div>

                              </div>

                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                  </div>

              </div>
      </div>
    </div>

    <script language="javascript" type="text/javascript">

        function Solo_Numerico(variable) {
            Numer = parseInt(variable);
            if (isNaN(Numer)) {
                return "";
            }
            return Numer;
        }
        function ValNumero(Control) {
            Control.value = Solo_Numerico(Control.value);
        }
    </script>
</asp:Content>
