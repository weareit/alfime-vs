﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="A_Registro.aspx.cs" Inherits="WebApplication1.Formulario_web11" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <%--Notificaciones--%>
    <div id="pnlNotificaciones" runat="server" visible="false">
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Error</strong> <asp:Label runat="server" Text="" Id="lblMensaje"></asp:Label>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>

<%--    Tarjeta de registro de Usuario del sistema--%>

    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Nuevo Usuario del Sistema</h4>
                  <p class="caxxrd-category">Por favor complete el perfil</p>
                </div>
                <div class="card-body">                  
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label class="bmd-label-floating">Numero de Identificación</label>
                          <asp:TextBox ID="txtIdentificacion" class="form-control" required="required" runat="server"></asp:TextBox>                          
                        </div>
                      </div>
                      <div class="col-md-7">
                        <div class="form-group">
                          <label class="bmd-label-floating">Tipo de documento</label>
                          <asp:DropDownList ID="ddlTipoDocumento" class="form-control" required="required" runat="server"></asp:DropDownList>                          
                        </div>
                      </div>    
                    </div>
                    <div class="row" style ="">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nombres</label>
                          <asp:TextBox ID="txtNombres" class="form-control" required="required" runat="server"></asp:TextBox>                          
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Apellidos</label>
                          <asp:TextBox ID="txtApellidos" class="form-control" required="required" runat="server"></asp:TextBox>                          
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Profesión</label>
                          <asp:DropDownList ID="ddlProfesion" class="form-control" required="required" runat="server"></asp:DropDownList>                          
                        </div>
                      </div>
                    </div>
                </div>
                  </div>
                </div>
              <div class="col-md-6">
              <div class="card">
                
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Relación con Alfime o Cargo</label>
                          <asp:DropDownList ID="ddlRelacion" class="form-control" required="required" runat="server"></asp:DropDownList>                          
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Télefono Fijo</label>
                          <asp:TextBox ID="txtTelefonoFijo" type="number" class="form-control" runat="server"></asp:TextBox>
                        </div>
                      </div>
                      <div class="col-md-6">
                            <div class="form-group">
                              <label class="bmd-label-floating">Télefono Celular</label>
                              <asp:TextBox ID="txtTelefonoCelular" type="number" class="form-control" runat="server"></asp:TextBox>
                            </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Correo Electronico</label>
                          <asp:TextBox ID="txtCorreo" type="email" class="form-control" runat="server"></asp:TextBox>
                        </div>
                      </div>                      
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                            <div class="form-group">
                              <label class="bmd-label-floating">Usuario</label>
                              <asp:TextBox ID="txtUsuario" class="form-control" required="required" runat="server"></asp:TextBox>
                            </div>
                      </div>
                      <div class="col-md-4">
                            <div class="form-group">
                              <label class="bmd-label-floating">Contraseña</label>
                              <asp:TextBox ID="txtContrasena" type="password" class="form-control" required="required" runat="server"></asp:TextBox>
                            </div>
                      </div>
                     <div class="col-md-4">
                            <div class="form-group">
                              <label class="bmd-label-floating">Repita su Contraseña</label>
                              <asp:TextBox ID="txtRContrasena" type="password" required="required" class="form-control" runat="server"></asp:TextBox>
                            </div>
                      </div>
                    </div>
                    <asp:Button runat="server" ID="btnRegistrar" class="btn btn-primary pull-right" Text="Registrarme" OnClick="btnRegistrar_Click" ></asp:Button>
                    <a href="Default.aspx" class="btn btn-success pull-right" >Cancelar</a>
                   
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
           
        </div>
      </div>
    </div>

</asp:Content>
