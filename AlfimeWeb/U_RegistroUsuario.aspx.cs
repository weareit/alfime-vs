﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Datos;

namespace WebApplication1
{
    public partial class Formulario_web12 : System.Web.UI.Page
    {
        #region Acceso Datos y Constructores
        Datos.DataSetBDAlfimeTableAdapters.TipoDocumentoesTableAdapter ControladorTipoDoc = new Datos.DataSetBDAlfimeTableAdapters.TipoDocumentoesTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.EPSTableAdapter ControladorEPS = new Datos.DataSetBDAlfimeTableAdapters.EPSTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter ControladorUsuario = new Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.InfoGeneraUsuarioTableAdapter ControladorInfoGeneral = new Datos.DataSetBDAlfimeTableAdapters.InfoGeneraUsuarioTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.TipoViviendaTableAdapter ControladorTipoVivienda = new Datos.DataSetBDAlfimeTableAdapters.TipoViviendaTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.ParentescoTableAdapter ControladorParentesco = new Datos.DataSetBDAlfimeTableAdapters.ParentescoTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.FondoPensionesTableAdapter ControladorFP = new Datos.DataSetBDAlfimeTableAdapters.FondoPensionesTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.CajasCompesacionTableAdapter ControladorCC = new Datos.DataSetBDAlfimeTableAdapters.CajasCompesacionTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.Nivel_AcademicoTableAdapter ControladorNA = new Datos.DataSetBDAlfimeTableAdapters.Nivel_AcademicoTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.SeguridadSocialTableAdapter ControladorSS = new Datos.DataSetBDAlfimeTableAdapters.SeguridadSocialTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.DepartamentoSelectCommandTableAdapter ControladorDep = new Datos.DataSetBDAlfimeTableAdapters.DepartamentoSelectCommandTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.MunicipioSelectCommandTableAdapter ControladorMun = new Datos.DataSetBDAlfimeTableAdapters.MunicipioSelectCommandTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.ContactoUsuarioTableAdapter ControladorCU = new Datos.DataSetBDAlfimeTableAdapters.ContactoUsuarioTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.FamiliarTableAdapter ControladorFamiliares = new Datos.DataSetBDAlfimeTableAdapters.FamiliarTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.SituacionFinancieraTableAdapter ControladorFinanzas = new Datos.DataSetBDAlfimeTableAdapters.SituacionFinancieraTableAdapter();


        DataTable familiares = new DataTable();
        #endregion

        string Mensaje = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["Login"] == null)
            {
                string cadena = HttpContext.Current.Request.Url.AbsoluteUri;
                if (!cadena.Contains("Default"))
                {
                    Response.Redirect("A_Acceso.aspx");
                }
            }
            if (!IsPostBack)
            {
                ddlTipoDocumento.DataSource = ControladorTipoDoc.GetData();
                ddlTipoDocumento.DataTextField = "Nombre";
                ddlTipoDocumento.DataValueField = "Id";
                ddlTipoDocumento.DataBind();

                ddlTipoIdentificacion.DataSource = ControladorTipoDoc.GetData();
                ddlTipoIdentificacion.DataTextField = "Nombre";
                ddlTipoIdentificacion.DataValueField = "Id";
                ddlTipoIdentificacion.DataBind();

                ddlEPS.DataSource = ControladorEPS.GetData();
                ddlEPS.DataTextField = "Nombre";
                ddlEPS.DataValueField = "Id";
                ddlEPS.DataBind();
                ddlEPS.Text = "No Aplica";

                ddlCajaS_Compensacion.DataSource = ControladorCC.GetData();
                ddlCajaS_Compensacion.DataTextField = "Nombre";
                ddlCajaS_Compensacion.DataValueField = "Id";
                ddlCajaS_Compensacion.DataBind();
                ddlCajaS_Compensacion.Text = "No Aplica";

                ddlFondo_Pensiones.DataSource = ControladorFP.GetData();
                ddlFondo_Pensiones.DataTextField = "Nombre";
                ddlFondo_Pensiones.DataValueField = "Id";
                ddlFondo_Pensiones.DataBind();
                ddlFondo_Pensiones.Text = "No Aplica";

                ddlParentesco.DataSource = ControladorParentesco.GetData();
                ddlParentesco.DataTextField = "Nombre";
                ddlParentesco.DataValueField = "Id";
                ddlParentesco.DataBind();
                ddlParentesco.Text = "No Aplica";

                ddlTipoVivienda.DataSource = ControladorTipoVivienda.GetData();
                ddlTipoVivienda.DataTextField = "Nombre";
                ddlTipoVivienda.DataValueField = "Id";
                ddlTipoVivienda.DataBind();
                ddlTipoVivienda.Text = "No Aplica";

                ddlNivelAcademico.DataSource = ControladorNA.GetData();
                ddlNivelAcademico.DataTextField = "Nivel";
                ddlNivelAcademico.DataValueField = "Id";
                ddlNivelAcademico.DataBind();
                ddlNivelAcademico.Text = "No Aplica";

                
                ddlDepartamento.DataSource = ControladorDep.GetData();
                ddlDepartamento.DataTextField = "Nombre";
                ddlDepartamento.DataValueField = "Codigo";
                ddlDepartamento.DataBind();

                ddlDepartamentoF.DataSource = ControladorDep.GetData();
                ddlDepartamentoF.DataTextField = "Nombre";
                ddlDepartamentoF.DataValueField = "Codigo";
                ddlDepartamentoF.DataBind();

                if (Session["IdMaster"] != null)
                {                    
                    CargarDatosPaciente((string)Session["IdMaster"]);
                    Session["IdMaster"] = null;
                }
            }

        }

        protected void btnPrueba_Click(object sender, EventArgs e)
        {
            pnlNotificaciones.Visible = true;
            lblMensaje.Text = chbHombre.Checked.ToString() + "" + chbMujer.Checked.ToString();
        }

        protected void btRegistrar_Click(object sender, EventArgs e)
        {
            
        }

        void agregarFamiliares()
        {
            
            familiares = Session["dtFamiliares"] != null ? (DataTable)Session["dtFamiliares"]:new DataTable() ;

            if (familiares.Columns.Count == 0)
            {
                familiares.Columns.Add("IdentificacionP", typeof(string));
                familiares.Columns.Add("IdentificacionA", typeof(string));
                familiares.Columns.Add("IdTipoDocumento", typeof(string));
                familiares.Columns.Add("Nombres", typeof(string));
                familiares.Columns.Add("Apellidos", typeof(string));
                familiares.Columns.Add("Edad", typeof(string));
                familiares.Columns.Add("Sexo", typeof(string));
                familiares.Columns.Add("Parentesco", typeof(string));
                familiares.Columns.Add("CodigoParentesco", typeof(string));
                familiares.Columns.Add("DepartamentoRes", typeof(string));
                familiares.Columns.Add("MunicipioRes", typeof(string));
                familiares.Columns.Add("BarrioRes", typeof(string));
                familiares.Columns.Add("DIreccionRes", typeof(string));
                familiares.Columns.Add("Telefono1", typeof(string));
                familiares.Columns.Add("Telefono2", typeof(string));
                familiares.Columns.Add("Acudiente", typeof(string));
            }
            bool existe = false;
            if (familiares.Rows.Count > 0)
            {
                existe = familiares.Select("IdentificacionA='" + txtidenificacionF.Text + "'").Count() > 0 ? true : false;
            }

            if (!existe)
            {
                DataRow row = familiares.NewRow();

                row["IdentificacionP"] = txtIdentificacion.Text;
                row["IdentificacionA"] = txtidenificacionF.Text;
                row["IdTipoDocumento"] = ddlTipoIdentificacion.SelectedValue.ToString();
                row["Nombres"] = txtNombresF.Text; 
                row["Apellidos"] = txtApellidosF.Text;
                row["Edad"] = txtEdadF.Text;
                row["Sexo"] = ddlSexo.SelectedValue.ToString();
                DataRow[] Parentesco = ControladorParentesco.GetData().Select("id='" + ddlParentesco.SelectedValue.ToString() + "'");
                row["Parentesco"] = Parentesco[0]["Nombre"].ToString();
                row["CodigoParentesco"] = ddlParentesco.SelectedValue.ToString();
                row["DepartamentoRes"] = ddlDepartamento.SelectedValue.ToString();
                row["MunicipioRes"] = ddlCiudadF.SelectedValue.ToString();
                row["BarrioRes"] = txtBarrioF.Text;
                row["DIreccionRes"] = txtDireccionF.Text;
                row["Telefono1"] = txtTelefonoF1.Text;
                row["Telefono2"] = txtTelefonoF2.Text;
                row["Acudiente"] = ddlAcudiente.Text;
                familiares.Rows.Add(row);

                Session["dtFamiliares"] = familiares;
                grvFamiliares.DataSource = (DataTable)Session["dtFamiliares"];
                grvFamiliares.DataBind();

                Mensaje ="" + Environment.NewLine;
                pnlNotificaciones.Visible = false;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('idAcuFamiliares')", true);
            }
            else
            {
                if (grvFamiliares.Rows.Count == 0)
                {
                    grvFamiliares.DataSource = (DataTable)Session["dtFamiliares"];
                    grvFamiliares.DataBind();
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                    Mensaje = Mensaje + "Ya se agregó un familiar con esta identificación, corrija la identificación o válide los datos del usuario!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('idAcuFamiliares')", true);
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                    Mensaje = Mensaje + "Ya se agregó un familiar con esta identificación, corrija la identificación o válide los datos del usuario!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('idAcuFamiliares')", true);
                }
            }

        }

        bool RegistrarUsuario()
        {
            try
            {
                if ((int)ControladorUsuario.ExisteUsuario(txtIdentificacion.Text) == 0)
                {
                    int paciente = ControladorUsuario.InsertarPaciente(
                       txtIdentificacion.Text,
                       Convert.ToInt32(ddlTipoIdentificacion.SelectedValue),
                       txtNombre1.Text,
                       txtNombre2.Text,
                       txtApellido1.Text,
                       txtApellido2.Text,
                       Convert.ToDateTime(dtpFechaNacimiento.Text),
                       chbHombre.Checked ? "Hombre" : "Mujer",
                       DateTime.Now,
                       DateTime.Now,
                       "123456",
                       "123456");
                    if (paciente > 0)
                    {
                       
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-success alert-dismissible fade show");
                        lblTituloMensaje.Text = "Información";
                        Mensaje = "El paciente ha sido registrado! continúe con los datos adicionales! " + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        btnCrearPaciente.Text = "Actualizar Datos Paciente";
                        txtIdentificacion.ReadOnly = true;

                        return true;
                    }
                    else
                    {
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                        Mensaje = "El paciente no pudo ser registrado debido a un error en el software contacte al administrador!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        return false;
                    }
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                    string Mensaje = "El paciente ya existe, por favor vaya al area de paciente y consultelo!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    return false;
                }

            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = "El paciente no pudo ser registrado debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }
            
        }

        bool ActualizarUsuario()
        {
            bool estado = false;
            try
            {
                if ((int)ControladorUsuario.ExisteUsuario(txtIdentificacion.Text) >= 0)
                {
                    int paciente = ControladorUsuario.ActualizarPaciente(
                      Convert.ToInt32(ddlTipoIdentificacion.SelectedValue),
                       txtNombre1.Text,
                       txtNombre2.Text,
                       txtApellido1.Text,
                       txtApellido2.Text,
                       Convert.ToDateTime(dtpFechaNacimiento.Text),
                       chbHombre.Checked ? "Hombre" : "Mujer",
                       DateTime.Now,
                       DateTime.Now,
                       "123456",
                       "123456",
                       txtIdentificacion.Text);
                    if (paciente > 0)
                    {
                        //alert - success
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-success alert-dismissible fade show");
                        lblTituloMensaje.Text = "Información";
                        Mensaje = "El paciente ha sido Actualizado! continúe con los datos adicionaleso o cambie de pagina! " + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        btnCrearPaciente.Text = "Actualizar Datos Paciente";
                        estado = true;
                    }
                    else
                    {
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                        Mensaje = "El paciente no pudo ser actualizado debido a un error en el software contacte al administrador!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        estado = false;
                    }
                }

                return estado;

            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = "El paciente no pudo ser actualizado debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                estado = false;
                return estado;
            }

        }

        bool RegistrarInformacionAdicional()
        {
            try
            {
                if (Convert.ToInt32(ControladorInfoGeneral.ExisteUsuarioIG(txtIdentificacion.Text)) == 0)
                {
                    int InfoGen = ControladorInfoGeneral.InfoGInsertCommand(
                        txtIdentificacion.Text,
                        txtOcupacion.Text,
                        Convert.ToInt32(ddlNivelAcademico.SelectedValue),
                        txtDxUsuario.Text,
                        ddlTipoDiscapacidad.SelectedValue.ToString(),
                        txtRemitente.Text,
                        txtReligion.Text);
                    if (InfoGen > 0)
                    {
                        return true;
                    }
                    else
                    {
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                        Mensaje = Mensaje + "La Información General no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        return false;
                    }
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                    Mensaje = Mensaje + "Ya se registro previamente la información general, por favor vaya al area de paciente y consultelo!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    return false;
                }
            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La infoemación General no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }
            
        }

        bool RegistrarSeguridadSocial()
        {
            try
            {
                if (Convert.ToInt32(ControladorSS.ExisteUsuarioSS(txtIdentificacion.Text)) == 0)
                {
                    int SS = ControladorSS.SeguridadSocialInsertCommand(
                        txtIdentificacion.Text,
                        ddlTipoAfiliacion.SelectedValue.ToString(),
                        Convert.ToInt32(ddlEPS.SelectedValue),
                        ddlCajaCompensacion.SelectedValue.ToString(),
                        Convert.ToInt32(ddlCajaS_Compensacion.SelectedValue),
                        ddlFondosPension.SelectedValue.ToString(),
                        Convert.ToInt32(ddlFondo_Pensiones.SelectedValue)
                        );
                    if (SS > 0)
                    {

                        return true;
                    }
                    else
                    {
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                        Mensaje = Mensaje + "La Información de Seguridad Social no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        return false;
                    }
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                    Mensaje = Mensaje +"Ya se registro previamente la información de Seguridad Social, por favor vaya al area de paciente y consultelo!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    return false;
                }
            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La Seguridad Social  no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        bool RegistrarContactoUsuario()
        {
            try
            {
                if (Convert.ToInt32(ControladorCU.ExisteUsuarioCU(txtIdentificacion.Text)) == 0)
                {
                    int CU = ControladorCU.ContactoUInsertCommand(
                        txtIdentificacion.Text,
                        ddlDepartamento.SelectedValue.ToString(),
                        ddlCiudad.SelectedValue.ToString(),
                        txtBarrio.Text,
                        txtDireccion.Text,
                        txtDireccion2.Text,
                        txtDireccion.Text,
                        txtTelefono.Text,
                        txtTelefono2.Text
                        );
                    if (CU > 0)
                    {

                        return true;
                    }
                    else
                    {
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                        Mensaje = Mensaje + "La Información de Contacto no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        return false;
                    }
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                    Mensaje = Mensaje + "Ya se registro previamente la información de Contacto del Paciente, por favor vaya al area de paciente y consultelo!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    return false;
                }
            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La Seguridad Social  no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        bool RegistraFamiliaresUsuario()
        {
            bool estado = false;
            try
            {
                if ((DataTable)Session["dtFamiliares"] != null)
                {
                    DataTable dtFamiliares = (DataTable)Session["dtFamiliares"];
                    if (dtFamiliares.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtFamiliares.Rows)
                        {
                            if (Convert.ToInt32(ControladorFamiliares.ExisteFamiliarUsuario(txtIdentificacion.Text, txtidenificacionF.Text)) == 0)
                            {

                                int CU = ControladorFamiliares.FamiliarInsertCommand(
                                    row["IdentificacionP"].ToString(),
                                    row["IdentificacionA"].ToString(),
                                    Convert.ToInt32(row["IdTipoDocumento"].ToString()),
                                    row["Nombres"].ToString(),
                                    row["Apellidos"].ToString(),
                                    Convert.ToInt32(row["Edad"].ToString()),
                                    row["Sexo"].ToString(),
                                    Convert.ToInt32(row["CodigoParentesco"].ToString()),
                                    row["Acudiente"].ToString(),
                                    row["DepartamentoRes"].ToString(),
                                    row["MunicipioRes"].ToString(),
                                    row["BarrioRes"].ToString(),
                                    row["DIreccionRes"].ToString(),
                                    row["Telefono1"].ToString(),
                                    row["Telefono2"].ToString()
                                    );
                                if (CU > 0)
                                {

                                    estado =  true;
                                    //Session["dtFamiliares"] = null;
                                }
                                else
                                {
                                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                                    Mensaje = Mensaje + "La Información Familiar de "+ row["Identificacion"].ToString()+" - "+
                                        row["Nombres"].ToString()+ " " +row["Apellidos"].ToString() + "no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                                    pnlNotificaciones.Visible = true;
                                    lblMensaje.Text = Mensaje;
                                    Mensaje = "";
                                    estado = false;
                                }
                            }
                            else
                            {
                                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                                Mensaje = Mensaje + "No se pudo registrar el Familiar " + row["Identificacion"].ToString()
                                    + " - " + row["Nombres"].ToString().ToUpper() + " " + row["Apellidos"].ToString().ToUpper() + " porque ya se encuentra registrado para este paciente." + Environment.NewLine;
                                pnlNotificaciones.Visible = true;
                                lblMensaje.Text = Mensaje;
                                Mensaje = "";
                                estado = false;

                            }
                        }
                    }
                    else
                    {
                        estado = false;
                    }

                }
                else
                {
                    estado = false;
                }

                return estado;
            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La información Familiar no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        bool RegistrarInfoFinanciera()
        {
            try
            {
                if (Convert.ToInt32(ControladorFinanzas.ExisteUsuFinanzas(txtIdentificacion.Text)) == 0)
                {
                    int CU = ControladorFinanzas.FinanzasInsertCommand(
                        txtIdentificacion.Text,
                        ddlDependencia.SelectedItem.Text,
                        txtPersonasCargo.Text,
                        chbPercibeIngresos.Checked ? "SI" : "NO",
                        Convert.ToInt64(txtVlrIngresos.Text),
                        chbSubsidio.Checked ? "SI" : "NO",
                        Convert.ToInt64(txtVlrSubsidio.Text),
                        chbActividadEco.Checked ? "SI" : "NO",
                        txtCualActividad.Text,
                        chbViviendaPropia.Checked? "SI" : "NO",
                        Convert.ToInt32(ddlTipoVivienda.SelectedValue.ToString())
                        );
                    if (CU > 0)
                    {

                        return true;
                    }
                    else
                    {
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                        Mensaje = Mensaje + "La Información Financiera  no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        return false;
                    }
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                    Mensaje = Mensaje + "Ya se registro previamente la información Financiera del Paciente, por favor vaya al area de paciente y consultelo!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    return false;
                }
            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La Información Financiera  no pudo ser registrada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        bool ActualizarInformacionAdicional()
        {
            try
            {
               
                    int InfoGen = ControladorInfoGeneral.ActualizarInfoGeneral(                        
                        txtOcupacion.Text,
                        Convert.ToInt32(ddlNivelAcademico.SelectedValue),
                        txtDxUsuario.Text,
                        ddlTipoDiscapacidad.SelectedValue.ToString(),
                        txtRemitente.Text,
                        txtReligion.Text,
                        txtIdentificacion.Text);
                    if (InfoGen > 0)
                    {
                        return true;
                    }
                    else
                    {
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                        Mensaje = Mensaje + "La Información General no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                        return false;
                    }
                
            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La información General no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        bool ActualizarSeguridadSocial()
        {
            try
            {

                int SS = ControladorSS.ActualizarSeguridadSocial(
                    ddlTipoAfiliacion.SelectedValue.ToString(),
                    Convert.ToInt32(ddlEPS.SelectedValue),
                    ddlCajaCompensacion.SelectedValue.ToString(),
                    Convert.ToInt32(ddlCajaS_Compensacion.SelectedValue),
                    ddlFondosPension.SelectedValue.ToString(),
                    Convert.ToInt32(ddlFondo_Pensiones.SelectedValue),
                    txtIdentificacion.Text
                    );
                if (SS > 0)
                {

                    return true;
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                    Mensaje = Mensaje + "La Información de Seguridad Social no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    return false;
                }

            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La Seguridad Social  no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        bool ActualizarContactoUsuario()
        {
            try
            {

                int CU = ControladorCU.ActualizarInfoContacto(
                    ddlDepartamento.SelectedValue.ToString(),
                    ddlCiudad.SelectedValue.ToString(),
                    txtBarrio.Text,
                    txtDireccion.Text,
                    txtDireccion2.Text,
                    txtTelefono.Text,
                    txtTelefono2.Text,
                    txtIdentificacion.Text
                    );
                if (CU > 0)
                {

                    return true;
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                    Mensaje = Mensaje + "La Información de Contacto no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    return false;
                }

            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La Seguridad Social  no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        bool ActualizarFamiliaresUsuario()
        {
            bool estado = false;
            try
            {
                if ((DataTable)Session["dtFamiliares"] != null)
                {
                    DataTable dtFamiliares = (DataTable)Session["dtFamiliares"];
                    if (dtFamiliares.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtFamiliares.Rows)
                        {

                            int CU = ControladorFamiliares.ActualizarFamiliar(                                
                                Convert.ToInt32(row["IdTipoDocumento"].ToString()),
                                row["Nombres"].ToString(),
                                row["Apellidos"].ToString(),
                                Convert.ToInt32(row["Edad"].ToString()),
                                row["Sexo"].ToString(),
                                Convert.ToInt32(row["CodigoParentesco"].ToString()),
                                row["Acudiente"].ToString(),
                                row["DepartamentoRes"].ToString(),
                                row["MunicipioRes"].ToString(),
                                row["BarrioRes"].ToString(),
                                row["DIreccionRes"].ToString(),
                                row["Telefono1"].ToString(),
                                row["Telefono2"].ToString(),
                                row["IdentificacionP"].ToString(),
                                row["IdentificacionA"].ToString()
                                );
                            if (CU > 0)
                            {

                                estado = true;
                                //Session["dtFamiliares"] = null;
                            }
                            else
                            {
                                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                                Mensaje = Mensaje + "La Información Familiar de " + row["Identificacion"].ToString() + " - " +
                                    row["Nombres"].ToString() + " " + row["Apellidos"].ToString() + "no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                                pnlNotificaciones.Visible = true;
                                lblMensaje.Text = Mensaje;
                                Mensaje = "";
                                estado = false;
                            }
                        }
                    }
                    else
                    {
                        estado = false;
                    }

                }
                else
                {
                    estado = false;
                }

                return estado;
            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La información Familiar no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        bool ActualizarInfoFinanciera()
        {
            try
            {

                int CU = ControladorFinanzas.ActualizarFinanzas(
                    ddlDependencia.SelectedItem.Text,
                    txtPersonasCargo.Text,
                    chbPercibeIngresos.Checked ? "SI" : "NO",
                    Convert.ToInt64(txtVlrIngresos.Text),
                    chbSubsidio.Checked ? "SI" : "NO",
                    Convert.ToInt64(txtVlrSubsidio.Text),
                    chbActividadEco.Checked ? "SI" : "NO",
                    txtCualActividad.Text,
                    chbViviendaPropia.Checked ? "SI" : "NO",
                    Convert.ToInt32(ddlTipoVivienda.SelectedValue.ToString()),
                    txtIdentificacion.Text
                    );
                if (CU > 0)
                {

                    return true;
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                    Mensaje = Mensaje + "La Información Financiera  no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    return false;
                }

            }
            catch (Exception e)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "La Información Financiera  no pudo ser Actualizada debido a un error en el software contacte al administrador!" + Environment.NewLine + e.ToString() + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                return false;
            }

        }

        protected void btnCrearPaciente_Click(object sender, EventArgs e)
        {
            if (btnCrearPaciente.Text.Contains("Crear"))
            {
                RegistrarUsuario();
            }
            else
            {
                ActualizarUsuario();
            }
        }
        
        protected void lnkAgregarFamiliar_Click(object sender, EventArgs e)
        {
            if (txtIdentificacion.Text.Length > 0)
            {
                if ((int)ControladorUsuario.ExisteUsuario(txtIdentificacion.Text) >0)
                {
                    agregarFamiliares();
                }
                else
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                    Mensaje = Mensaje + "No hay un paciente activo o creado!" + Environment.NewLine;
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                    ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('idAcuFamiliares')", true);
                }
            }
            else
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                Mensaje = Mensaje + "No hay un paciente activo o creado!" + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                Mensaje = "";
                ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('idAcuFamiliares')", true);
            }
        }

        protected void grvFamiliares_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            familiares = (DataTable)Session["dtFamiliares"];
            if (e.CommandName == "Select")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string id = Convert.ToString(grvFamiliares.DataKeys[index].Value);

                string Consulta = "IdentificacionA='" + id +"'";
                DataRow[] drr = familiares.Select(Consulta);
                for (int i = 0; i < drr.Length; i++)
                    familiares.Rows.Remove(drr[i]);
                    familiares.AcceptChanges();

                Session["dtFamiliares"] = familiares;

                grvFamiliares.DataSource = (DataTable)Session["dtFamiliares"];
                grvFamiliares.DataBind();

                ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('idAcuFamiliares')", true);
            }
        }

        protected void grvFamiliares_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            string codigo = ddlDepartamento.SelectedValue.ToString();

            ddlCiudad.DataSource = ControladorMun.GetData(codigo);
            ddlCiudad.DataTextField = "Nombre";
            ddlCiudad.DataValueField = "Codigo";
            ddlCiudad.DataBind();

        }

        protected void ddlDepartamentoF_SelectedIndexChanged(object sender, EventArgs e)
        {
            string codigo = ddlDepartamentoF.SelectedValue.ToString();

            ddlCiudadF.DataSource = ControladorMun.GetData(codigo);
            ddlCiudadF.DataTextField = "Nombre";
            ddlCiudadF.DataValueField = "Codigo";
            ddlCiudadF.DataBind();
            ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('idAcuFamiliares')", true);

        }

        protected void txtIdentificacion_TextChanged(object sender, EventArgs e)
        {
            DataTable dtPaciente = ControladorUsuario.dtObtenerPaciente(txtIdentificacion.Text);
            if (dtPaciente != null && dtPaciente.Rows.Count>0)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                Mensaje = Mensaje + "El paciente ya existe, se consultará su información!" + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;                
                Mensaje = "";
                //Consultar paciente y cargar datos
                txtNombre1.Text =   dtPaciente.Rows[0]["PrimerNombre"].ToString();
                txtNombre2.Text =   dtPaciente.Rows[0]["SegundoNombre"].ToString();
                txtApellido1.Text = dtPaciente.Rows[0]["PrimerApellido"].ToString();
                txtApellido2.Text = dtPaciente.Rows[0]["SegundoApellido"].ToString();
                DateTime FechaN = Convert.ToDateTime(dtPaciente.Rows[0]["FechaNacimiento"].ToString());
                dtpFechaNacimiento.Text = String.Format("{0:yyyy-MM-dd}", FechaN);

                if (dtPaciente.Rows[0]["Sexo"].ToString() == "Hombre")
                    chbHombre.Checked = true;
                else
                    chbMujer.Checked = true;
                ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('InfoAdicional')", true);
                btnCrearPaciente.Text = "Actualizar Datos Paciente";
            }

            CargarDatosPaciente(txtIdentificacion.Text);
        }



        protected void btnAgregarInfoAdicional_Click(object sender, EventArgs e)
        {
            if (ValidarCamposAdicionales())
            {
                if (btnAgregarInfoAdicional.Text.Contains("Agregar"))
                {
                    bool estado = false;
                    if (RegistrarInformacionAdicional()) { estado = true; }
                    if (RegistrarSeguridadSocial()) { estado = true; }
                    if (RegistrarContactoUsuario()) { estado = true; }
                    if (RegistraFamiliaresUsuario()) { Session["dtFamiliares"] = null; estado = true; }
                    if (RegistrarInfoFinanciera()) { estado = true; }

                    if (estado)
                        Response.Redirect("U_Usuario.aspx");
                }
                else
                {
                    bool estado = false;
                    if (ActualizarInformacionAdicional()) { estado = true; }
                    if (ActualizarSeguridadSocial()) { estado = true; }
                    if (ActualizarContactoUsuario()) { estado = true; }
                    if (ActualizarFamiliaresUsuario()) { estado = true; }
                    if (ActualizarInfoFinanciera()) { estado = true; }

                    if (estado)
                    {
                        pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-success alert-dismissible fade show");
                        Mensaje = Mensaje + "La informaciòn el Paciente fue actualizada Correctamente!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                    }
                        
                }

            }
            
        }

        bool ValidarCamposAdicionales()
        {
            try
            {
                Mensaje = "";
                familiares = (DataTable)Session["dtFamiliares"];
                Mensaje = Mensaje + (string.IsNullOrEmpty(txtTelefono.Text.Trim()) ? "- Falta el teléfono del paciente" + Environment.NewLine : "");
                Mensaje = Mensaje + (string.IsNullOrEmpty(ddlCiudad.Text.Trim()) ? "- Falta la ciudad de residencia del paciente" + Environment.NewLine : "");
                Mensaje = Mensaje + (string.IsNullOrEmpty(txtBarrio.Text.Trim()) ? "- Falta el barrio de residencia del paciente" + Environment.NewLine : "");
                Mensaje = Mensaje + (string.IsNullOrEmpty(txtDireccion.Text.Trim()) ? "- Falta la dirección de residencia del paciente" + Environment.NewLine : "");
                Mensaje = Mensaje + (string.IsNullOrEmpty(txtOcupacion.Text.Trim()) ? "- Falta la ocupación del paciente" + Environment.NewLine : "");
                Mensaje = Mensaje + (string.IsNullOrEmpty(txtDxUsuario.Text.Trim()) ? "- Falta el diagnóstico percibido del paciente" + Environment.NewLine : "");
                Mensaje = Mensaje + (string.IsNullOrEmpty(ddlTipoDiscapacidad.Text.Trim()) ? "- Falta el teléfono del paciente" + Environment.NewLine : "");
                Mensaje = Mensaje + (string.IsNullOrEmpty(txtRemitente.Text.Trim()) ? "- Falta quien remite al paciente" + Environment.NewLine : "");
                Mensaje = Mensaje + (string.IsNullOrEmpty(ddlTipoAfiliacion.Text.Trim()) ? "- Falta el tipo de Afiliación del paciente" + Environment.NewLine : "");
                if ((ddlTipoAfiliacion.SelectedItem.Text == "Sin afiliación" || ddlTipoAfiliacion.SelectedItem.Text == "SISBEN") && ddlEPS.SelectedItem.Text.Trim() != "No Aplica")
                {
                    Mensaje = Mensaje + "- Por favor seleccione la opción no aplica en EPS o cambie el tipo de afiliación " + Environment.NewLine;
                }
                if ((ddlTipoAfiliacion.SelectedItem.Text != "Sin afiliación" || ddlTipoAfiliacion.SelectedItem.Text != "SISBEN") && ddlEPS.Text.Trim() == "No Aplica")
                {
                    Mensaje = Mensaje + "- Por favor seleccione una opción valida en EPS o cambie el tipo de afiliación " + Environment.NewLine;
                }
                Mensaje = Mensaje + (string.IsNullOrEmpty(ddlCajaCompensacion.Text.Trim()) ? "- Debe seleccionar si el paciente tiene Caja de Compensación!" + Environment.NewLine : "");
                if (ddlCajaCompensacion.SelectedItem.Text == "SI" && ddlCajaS_Compensacion.SelectedItem.Text == "No Aplica")
                {
                    Mensaje = Mensaje + "- Por favor seleccione una opción diferente a no aplica en en cual es la Caja de Compensacíón" + Environment.NewLine;
                }
                if (ddlCajaCompensacion.SelectedItem.Text == "NO" && ddlCajaS_Compensacion.SelectedItem.Text != "No Aplica")
                {
                    Mensaje = Mensaje + "- Por favor seleccione No Aplica en cual la Caja de Compensacíón" + Environment.NewLine;
                }
                Mensaje = Mensaje + (string.IsNullOrEmpty(ddlFondosPension.Text.Trim()) ? "- Debe seleccionar si el cliente tiene Fondo de Pensiones" + Environment.NewLine : "");
                if (ddlFondosPension.SelectedItem.Text == "SI" && ddlFondo_Pensiones.SelectedItem.Text == "No Aplica")
                {
                    Mensaje = Mensaje + "- Por favor seleccione una opción diferente a no aplica en en cual es el Fondo de Pensiones" + Environment.NewLine;
                }
                if (ddlFondosPension.SelectedItem.Text == "NO" && ddlFondo_Pensiones.SelectedItem.Text != "No Aplica")
                {
                    Mensaje = Mensaje + "- Por favor seleccione No Aplica en cual es el Fondo de Pensiones" + Environment.NewLine;
                }
                if (familiares != null && familiares.Rows.Count == 0)
                {
                    Mensaje = Mensaje + "- Debe existir al menos un familiar/acudiente para el paciente!" + Environment.NewLine;
                }
                if (familiares.Rows.Count == 1)
                {
                    if (familiares.Rows[0]["Acudiente"].ToString() != "SI")
                    {
                        Mensaje = Mensaje + "- Debe seleccionar la persona registrada como acudiente!" + Environment.NewLine;
                    }
                }
                if (chbPercibeIngresos.Checked && (txtVlrIngresos.Text.Trim() == "0" || txtVlrIngresos.Text.Trim() == ""))
                {
                    Mensaje = Mensaje + "- Debe ingresar un valor de ingresos mayor a cero (0)!" + Environment.NewLine;
                }
                if (!chbPercibeIngresos.Checked && txtVlrIngresos.Text.Trim() != "0")
                {
                    Mensaje = Mensaje + "- Debe ingresar un valor de ingresos igual a (0)!" + Environment.NewLine;
                }
                if (chbSubsidio.Checked && (txtVlrSubsidio.Text.Trim() == "0" || txtVlrSubsidio.Text.Trim() == ""))
                {
                    Mensaje = Mensaje + "- Debe ingresar un valor de subsidios mayor a cero (0)!" + Environment.NewLine;
                }
                if (!chbSubsidio.Checked && txtVlrSubsidio.Text.Trim() != "0")
                {
                    Mensaje = Mensaje + "- Debe ingresar un valor de subsidios igual a cero (0)!" + Environment.NewLine;
                }
                if (chbActividadEco.Checked && (txtCualActividad.Text == "Ninguna" || string.IsNullOrEmpty(txtCualActividad.Text.Trim())))
                {
                    Mensaje = Mensaje + "- Debe ingresar cual es su actividad económica)!" + Environment.NewLine;
                }
                if (chbViviendaPropia.Checked && (ddlTipoVivienda.SelectedItem.Text != "No Aplica"))
                {
                    Mensaje = Mensaje + "- Debe seleccionar la opciòn No Aplica en Tipo de Vivienda!" + Environment.NewLine;
                }
                if (!chbViviendaPropia.Checked && (ddlTipoVivienda.SelectedItem.Text == "No Aplica"))
                {
                    Mensaje = Mensaje + "- Debe seleccionar la opción No Aplica como tipo de vivienda!" + Environment.NewLine;
                }

                if (Mensaje.Trim() != "")
                {
                    pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                }


                if (string.IsNullOrEmpty(Mensaje.Trim()))
                    return true;
                else
                    return false;
            }
            catch(Exception e)
            {
                Mensaje = Mensaje+ "- Error al realizar la validación de los campos, contacte al administrador del sistema!"+Environment.NewLine;
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                return false;
            }
            
        }


        void CargarDatosPaciente(string IdentificacionPaciente)
        {            

            ConsultarPaciente(IdentificacionPaciente);
            ConsultarInformacionGeneralPaciente(IdentificacionPaciente);
            ConsultarDatosContactopaciente(IdentificacionPaciente);
            ConsultarDatosFamiliares(IdentificacionPaciente);
            ConsultarInformacionFinanciera(IdentificacionPaciente);
            ConsultarInfoSeguridadSocial(IdentificacionPaciente);
            btnAgregarInfoAdicional.Text = "Actualizar Información Adicional";
        }

        void ConsultarPaciente(string Identificacion)
        {
            DataTable dtPaciente = ControladorUsuario.dtObtenerPaciente(Identificacion);
            if (dtPaciente != null && dtPaciente.Rows.Count > 0)
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-warning alert-dismissible fade show");
                Mensaje = Mensaje + "Modo de Actualización!" + Environment.NewLine;
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                lblTituloMensaje.Text = "";
                Mensaje = "";
                //Consultar paciente y cargar datos
                
                txtIdentificacion.Text= dtPaciente.Rows[0]["Identificacion"].ToString();
                txtIdentificacion.ReadOnly = true;
                txtNombre1.Text = dtPaciente.Rows[0]["PrimerNombre"].ToString();
                txtNombre2.Text = dtPaciente.Rows[0]["SegundoNombre"].ToString();
                txtApellido1.Text = dtPaciente.Rows[0]["PrimerApellido"].ToString();
                txtApellido2.Text = dtPaciente.Rows[0]["SegundoApellido"].ToString();
                DateTime FechaN = Convert.ToDateTime(dtPaciente.Rows[0]["FechaNacimiento"].ToString());
                dtpFechaNacimiento.Text = String.Format("{0:yyyy-MM-dd}", FechaN);

                if (dtPaciente.Rows[0]["Sexo"].ToString() == "Hombre")
                    chbHombre.Checked = true;
                else
                    chbMujer.Checked = true;
                ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('InfoAdicional')", true);
                btnCrearPaciente.Text = "Actualizar Datos Paciente";
            }
        }

        void ConsultarInformacionGeneralPaciente(string Identificacion)
        {
            DataTable dtinfogeneral = ControladorInfoGeneral.dtInfoGralPaciente(Identificacion);
            if (dtinfogeneral != null && dtinfogeneral.Rows.Count > 0)
            {
                
                //Consultar paciente y cargar datos
                txtOcupacion.Text = dtinfogeneral.Rows[0]["Ocupacion"].ToString();
                ddlNivelAcademico.SelectedValue = dtinfogeneral.Rows[0]["Nivel_Academico"].ToString();
                txtDxUsuario.Text = dtinfogeneral.Rows[0]["DxSegunUsuario"].ToString();
                ddlTipoDiscapacidad.SelectedValue = dtinfogeneral.Rows[0]["TipoDiscapacidad"].ToString();
                txtRemitente.Text = dtinfogeneral.Rows[0]["Remitente"].ToString();
                txtReligion.Text = dtinfogeneral.Rows[0]["Religión"].ToString();
            }
        }

        void ConsultarDatosContactopaciente(string Identificacion)
        {
            DataTable dtContacto = ControladorCU.dtConsultaContacPaciente(Identificacion);
            if (dtContacto != null && dtContacto.Rows.Count > 0)
            {
                //Consultar paciente y cargar datos
                txtTelefono.Text = dtContacto.Rows[0]["Telefono1"].ToString();
                txtTelefono2.Text = dtContacto.Rows[0]["Telefono2"].ToString();
                ddlDepartamento.SelectedValue = dtContacto.Rows[0]["DepartamentoRes"].ToString();
                string codigo = ddlDepartamento.SelectedValue.ToString();
                ddlCiudad.DataSource = ControladorMun.GetData(codigo);
                ddlCiudad.DataTextField = "Nombre";
                ddlCiudad.DataValueField = "Codigo";
                ddlCiudad.DataBind();
                ddlCiudad.SelectedValue = dtContacto.Rows[0]["MunicipioRes"].ToString();
                txtBarrio.Text = dtContacto.Rows[0]["BarrioRes"].ToString();
                txtDireccion.Text = dtContacto.Rows[0]["Direccion1"].ToString();
                txtDireccion2.Text = dtContacto.Rows[0]["DireccionAdicional"].ToString();
            }
        }

        void ConsultarDatosFamiliares(string Identificacion)
        {
            DataTable dtFamiliares = ControladorFamiliares.dtConsultarFamiPaciente(Identificacion);
            if (dtFamiliares != null && dtFamiliares.Rows.Count > 0)
            {
                Session["dtFamiliares"] = dtFamiliares;
                grvFamiliares.DataSource = (DataTable)Session["dtFamiliares"];
                grvFamiliares.DataBind();
            }
        }

        void ConsultarInformacionFinanciera(string Identificacion)
        {
            DataTable dtFinanzas = ControladorFinanzas.dtFinanzasUsuario(Identificacion);
            if (dtFinanzas != null && dtFinanzas.Rows.Count > 0)
            {
                //Consultar paciente y cargar datos
                ddlDependencia.SelectedValue = dtFinanzas.Rows[0]["Dependencia"].ToString();
                txtPersonasCargo.Text = dtFinanzas.Rows[0]["Personas_a_Cargo"].ToString();
                chbPercibeIngresos.Checked = dtFinanzas.Rows[0]["PercibeIngresos"].ToString() == "SI" ? true:false ;
                txtVlrIngresos.Text = dtFinanzas.Rows[0]["Ingresos"].ToString();
                chbSubsidio.Checked = dtFinanzas.Rows[0]["SubsidioEconimico"].ToString() == "SI" ? true : false;
                txtVlrSubsidio.Text = dtFinanzas.Rows[0]["MontoSubsidio"].ToString();
                chbActividadEco.Checked = dtFinanzas.Rows[0]["ActividadEconomica"].ToString() == "SI" ? true : false;
                txtCualActividad.Text = dtFinanzas.Rows[0]["CualActividad"].ToString();
                chbViviendaPropia.Checked = dtFinanzas.Rows[0]["ViviendaPropia"].ToString() == "SI" ? true : false;
                ddlTipoVivienda.SelectedValue = dtFinanzas.Rows[0]["TipoVivienda"].ToString();
            }
        }

        void ConsultarInfoSeguridadSocial(string Identificacion)
        {
            DataTable dtSeguridadSocial = ControladorSS.dtConsultarSeguridadS(Identificacion);
            if (dtSeguridadSocial != null && dtSeguridadSocial.Rows.Count > 0)
            {
                //Consultar paciente y cargar datos
                ddlTipoAfiliacion.SelectedValue = dtSeguridadSocial.Rows[0]["TipoAfiliacionSalud"].ToString();
                ddlEPS.SelectedValue = dtSeguridadSocial.Rows[0]["CualEPS"].ToString();
                ddlCajaCompensacion.SelectedValue = dtSeguridadSocial.Rows[0]["CajaCompensacion"].ToString();
                ddlCajaS_Compensacion.SelectedValue = dtSeguridadSocial.Rows[0]["CualCaja"].ToString();
                ddlFondosPension.SelectedValue = dtSeguridadSocial.Rows[0]["FondoPensiones"].ToString();
                ddlFondo_Pensiones.SelectedValue = dtSeguridadSocial.Rows[0]["CualFondo"].ToString();
            }
        }
    }    
}