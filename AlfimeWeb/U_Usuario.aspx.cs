﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Datos;

namespace WebApplication1
{
    public partial class Formulario_web15 : System.Web.UI.Page
    {
        #region Acceso Datos y Constructores
        Datos.DataSetBDAlfimeTableAdapters.TipoDocumentoesTableAdapter ControladorTipoDoc = new Datos.DataSetBDAlfimeTableAdapters.TipoDocumentoesTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.EPSTableAdapter ControladorEPS = new Datos.DataSetBDAlfimeTableAdapters.EPSTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter ControladorUsuario = new Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.InfoGeneraUsuarioTableAdapter ControladorInfoGeneral = new Datos.DataSetBDAlfimeTableAdapters.InfoGeneraUsuarioTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.TipoViviendaTableAdapter ControladorTipoVivienda = new Datos.DataSetBDAlfimeTableAdapters.TipoViviendaTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.ParentescoTableAdapter ControladorParentesco = new Datos.DataSetBDAlfimeTableAdapters.ParentescoTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.FondoPensionesTableAdapter ControladorFP = new Datos.DataSetBDAlfimeTableAdapters.FondoPensionesTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.CajasCompesacionTableAdapter ControladorCC = new Datos.DataSetBDAlfimeTableAdapters.CajasCompesacionTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.Nivel_AcademicoTableAdapter ControladorNA = new Datos.DataSetBDAlfimeTableAdapters.Nivel_AcademicoTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.SeguridadSocialTableAdapter ControladorSS = new Datos.DataSetBDAlfimeTableAdapters.SeguridadSocialTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.DepartamentoSelectCommandTableAdapter ControladorDep = new Datos.DataSetBDAlfimeTableAdapters.DepartamentoSelectCommandTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.MunicipioSelectCommandTableAdapter ControladorMun = new Datos.DataSetBDAlfimeTableAdapters.MunicipioSelectCommandTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.ContactoUsuarioTableAdapter ControladorCU = new Datos.DataSetBDAlfimeTableAdapters.ContactoUsuarioTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.FamiliarTableAdapter ControladorFamiliares = new Datos.DataSetBDAlfimeTableAdapters.FamiliarTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.SituacionFinancieraTableAdapter ControladorFinanzas = new Datos.DataSetBDAlfimeTableAdapters.SituacionFinancieraTableAdapter();
        Datos.dsAlfimeAspectosTableAdapters.AspectoLaboralTableAdapter ControladorAL = new Datos.dsAlfimeAspectosTableAdapters.AspectoLaboralTableAdapter();
        Datos.dsAlfimeAspectosTableAdapters.AspectoMedicoTableAdapter ControladorAM = new Datos.dsAlfimeAspectosTableAdapters.AspectoMedicoTableAdapter();
        Datos.dsAlfimeAspectosTableAdapters.AspectoSicologicoTableAdapter ControladorAS = new Datos.dsAlfimeAspectosTableAdapters.AspectoSicologicoTableAdapter();
        Datos.dsAlfimeAspectosTableAdapters.AspectoVidaDiariaTableAdapter ControladorAV = new Datos.dsAlfimeAspectosTableAdapters.AspectoVidaDiariaTableAdapter();

        string Mensaje = string.Empty;
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] == null)
            {
                string cadena = HttpContext.Current.Request.Url.AbsoluteUri;
                if (!cadena.Contains("Default"))
                {
                    Response.Redirect("A_Acceso.aspx");
                }
            }
            if (!IsPostBack)
            {
                pnlGeneral.Visible = false;
                
                if (Session["IdMaster"] != null)
                {
                    CargarDatosPaciente((string)Session["IdMaster"]);
                    //Session["Usuario"] = null;
                    //Session["IdMaster"] = null;
                }                
                if (Session["Usuario"] != null)
                {
                    DataTable Usu = (DataTable)Session["Usuario"];
                    CargarDatosPaciente(Usu.Rows[0]["Identificacion"].ToString());
                }

                

            }


            
            //ClientScript.RegisterStartupScript(GetType(), "IrTab", "moverseA('Aspectos')", true);

        }

        #region Pacientes
        void CargarDatosPaciente(string identificacion)
        {

            DataTable dtPaciente = ControladorUsuario.dtObtenerPaciente(identificacion);
            DataTable dtContactoPaciente = ControladorCU.dtConsultaContacPaciente(identificacion);
            DataTable dtInfoGeneral = ControladorInfoGeneral.dtInfoGralPaciente(identificacion);
            DataTable dtSeguridadSocial = ControladorSS.dtConsultarSeguridadS(identificacion);
            DataTable dtFamilia = ControladorFamiliares.dtConsultarFamiPaciente(identificacion);
            DataTable dtFinanzas = ControladorFinanzas.dtFinanzasUsuario(identificacion);

            if (dtPaciente != null && dtPaciente.Rows.Count > 0)
            {
                
               pnlGeneral.Visible = true;
               
                
                lblNombreCompletoValor.Text =
                    dtPaciente.Rows[0]["PrimerNombre"].ToString().ToUpper() + " " + dtPaciente.Rows[0]["SegundoNombre"].ToString().ToUpper() + " " +
                     dtPaciente.Rows[0]["PrimerApellido"].ToString().ToUpper() +" "+
                     dtPaciente.Rows[0]["SegundoApellido"].ToString().ToUpper();
                lblpaciente.Text = lblNombreCompletoValor.Text;
                lblFechaNacimiento.Text = (dtPaciente.Rows[0]["FechaNacimiento"].ToString());
                lblFechaNacimiento.Text = lblFechaNacimiento.Text.Split(' ')[0];
                lblSexo.Text = dtPaciente.Rows[0]["Sexo"].ToString().ToUpper();
                lblIdentificacionValor.Text = dtPaciente.Rows[0]["Identificacion"].ToString().ToUpper();
                DataRow[] TipoId = ControladorTipoDoc.GetData().Select("id='" + dtPaciente.Rows[0]["IdTipoDocumento"].ToString() + "'");
                lblTipoIdentificacion.Text = TipoId[0]["Nombre"].ToString().ToUpper();

                //Información Contacto
                lblDireccion.Text =
                    dtContactoPaciente.Rows[0]["Direccion1"].ToString() + " - " +
                    dtContactoPaciente.Rows[0]["DireccionAdicional"].ToString();
                lblBarrio.Text = dtContactoPaciente.Rows[0]["BarrioRes"].ToString();
                lblTelefonos.Text = dtContactoPaciente.Rows[0]["Telefono1"].ToString() + " - " + dtContactoPaciente.Rows[0]["Telefono2"].ToString();
                DataRow[] codDepar = ControladorDep.GetData().Select("Codigo='" + dtContactoPaciente.Rows[0]["DepartamentoRes"].ToString() + "'");
                lblDepartamentos.Text = codDepar[0]["Nombre"].ToString();
                DataRow[] codMunicipio = ControladorMun.GetData(dtContactoPaciente.Rows[0]["DepartamentoRes"].ToString()).Select(
                    "Codigo='" + dtContactoPaciente.Rows[0]["MunicipioRes"].ToString() + "'");
                lblMunicipio.Text = codMunicipio[0]["Nombre"].ToString();

                //Información General
                lblOcupacion.Text = dtInfoGeneral.Rows[0]["Ocupacion"].ToString();
                DataRow[] NivAca = ControladorNA.GetData().Select("Id='" + dtInfoGeneral.Rows[0]["Nivel_Academico"].ToString() + "'");
                lblNivelAcademico.Text = NivAca[0]["Nivel"].ToString();
                lblDx.Text = dtInfoGeneral.Rows[0]["DxSegunUsuario"].ToString();
                lblDiscapacidad.Text = dtInfoGeneral.Rows[0]["TipoDiscapacidad"].ToString();
                lblRemitente.Text = dtInfoGeneral.Rows[0]["Remitente"].ToString();
                lblReligion.Text = dtInfoGeneral.Rows[0]["Religión"].ToString();

                //Seguridad Social
                lblTipoAfiliacion.Text = dtSeguridadSocial.Rows[0]["TipoAfiliacionSalud"].ToString();
                DataRow[] EPS = ControladorEPS.GetData().Select("Id='" + dtSeguridadSocial.Rows[0]["CualEPS"].ToString() + "'");
                lblEPS.Text = EPS[0]["Nombre"].ToString();
                DataRow[] Caja = ControladorCC.GetData().Select("Id='" + dtSeguridadSocial.Rows[0]["CualCaja"].ToString() + "'");
                lblCaja.Text = Caja[0]["Nombre"].ToString();
                DataRow[] Fondo = ControladorCC.GetData().Select("Id='" + dtSeguridadSocial.Rows[0]["CualFondo"].ToString() + "'");
                lblFondoPensiones.Text = Fondo[0]["Nombre"].ToString();

                //Familiares
                DataTable newTabla = dtFamilia.Copy();

                DataColumn ParentescoNombre = new DataColumn("ParentescoDesc", typeof(string));
                newTabla.Columns.Add(ParentescoNombre);
                newTabla.AcceptChanges();
                for (int i = 0; i < newTabla.Rows.Count; i++)
                {
                    string Paren = newTabla.Rows[i]["Parentesco"].ToString();
                    DataRow[] Parentesco = ControladorParentesco.GetData().Select("Id='" + Paren + "'");
                    newTabla.Rows[i]["ParentescoDesc"] = Parentesco[0]["Nombre"].ToString();
                }
                grvFamiliares.DataSource = newTabla;
                grvFamiliares.DataBind();

                //Financiera
                lblDepencencia.Text = dtFinanzas.Rows[0]["Dependencia"].ToString();
                lblPersonasCargo.Text = dtFinanzas.Rows[0]["Personas_a_cargo"].ToString();
                lblPercibe.Text = dtFinanzas.Rows[0]["PercibeIngresos"].ToString();
                lblIngresos.Text = dtFinanzas.Rows[0]["Ingresos"].ToString();
                lblPercibeSubsidios.Text = dtFinanzas.Rows[0]["SubsidioEconimico"].ToString();
                lblSubsidios.Text = dtFinanzas.Rows[0]["MontoSubsidio"].ToString();
                lblRealizaActividad.Text = dtFinanzas.Rows[0]["ActividadEconomica"].ToString();
                lblCualActividad.Text = dtFinanzas.Rows[0]["CualActividad"].ToString();
                lblViviendaPropia.Text = dtFinanzas.Rows[0]["ViviendaPropia"].ToString();
                DataRow[] Viv = ControladorTipoVivienda.GetData().Select("Id='" + dtFinanzas.Rows[0]["TipoVivienda"].ToString() + "'");
                lblTipoVivienda.Text = Viv[0]["Nombre"].ToString();

            }
            else
            {
                string Mensaje = "No éxiste el Paciente por favor valide la identificación o Registrelo!";
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                lblTituloMensaje.Text = "Error";
                Mensaje = "";
            }

        }

        private DataTable GetTransposedTable(DataTable dt)
        {
            DataTable newTable = new DataTable();
            newTable.Columns.Add(new DataColumn("0", typeof(string)));
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                DataRow newRow = newTable.NewRow();
                newRow[0] = dt.Columns[i].ColumnName;
                for (int j = 1; j <= dt.Rows.Count; j++)
                {
                    if (newTable.Columns.Count < dt.Rows.Count + 1)
                        newTable.Columns.Add(new DataColumn(j.ToString(), typeof(string)));
                    newRow[j] = dt.Rows[j - 1][i];
                }
                newTable.Rows.Add(newRow);
            }
            return newTable;
        }

        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = inRow[0].ToString();
                outputTable.Columns.Add(newColName);
            }
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }
            return outputTable;
        }

        protected string ExportDatatableToHtml(DataTable dt)
        {
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<table border='1px' cellpadding='1' cellspacing='1' bgcolor='lightyellow' style='font-family:Garamond; font-size:smaller'>");

            strHTMLBuilder.Append("<tr >");
            foreach (DataColumn myColumn in dt.Columns)
            {
                strHTMLBuilder.Append("<td >");
                strHTMLBuilder.Append(myColumn.ColumnName);
                strHTMLBuilder.Append("</td>");
            }
            strHTMLBuilder.Append("</tr>");


            foreach (DataRow myRow in dt.Rows)
            {

                strHTMLBuilder.Append("<tr >");
                foreach (DataColumn myColumn in dt.Columns)
                {
                    strHTMLBuilder.Append("<td >");
                    strHTMLBuilder.Append(myRow[myColumn.ColumnName].ToString());
                    strHTMLBuilder.Append("</td>");

                }
                strHTMLBuilder.Append("</tr>");
            }

            //Close tags.  
            strHTMLBuilder.Append("</table>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;

        }

        protected void btnActualizarInfo_Click(object sender, EventArgs e)
        {
            Response.Redirect("U_RegistroUsuario.aspx");
        }

        #endregion

        bool ValidarAM()
        {

            Mensaje = "";
            Mensaje = Mensaje + (string.IsNullOrEmpty(txtDxMedico.Text.Trim()) ? "- Falta el Diagnóstico Médico del paciente" + Environment.NewLine : "");
            string Dia = ""; string Meses = ""; string Anos = "";
            Dia = (!string.IsNullOrEmpty(txtDiasD.Text.Trim()) ? txtDiasD.Text + "día's; " : "");
            Meses = (!string.IsNullOrEmpty(txtMesesD.Text.Trim()) ? txtMesesD.Text + "Mes'es; " : "");
            Anos = (!string.IsNullOrEmpty(txtMesesD.Text.Trim()) ? txtAnosD.Text + "Año's; " : "");
            Mensaje = Mensaje + (string.IsNullOrEmpty((Dia + Meses + Anos).Trim()) ? "- Falta el tiempo de discapacidad" + Environment.NewLine : "");
            Mensaje = Mensaje + (string.IsNullOrEmpty(txtAntecedentesM.Text.Trim()) ? "- Faltan los antecedentes Familiares, esccribir Ninguno en caso de no existir!" + Environment.NewLine : "");
            Mensaje = Mensaje + (string.IsNullOrEmpty(txtRecomendacionesM.Text.Trim()) ? "- Faltan las Recomendaciones Medicas, esccribir Ninguna en caso de no existir!" + Environment.NewLine : "");

            if (Mensaje.Trim() != "")
            {
                pnlContenedorNotificaciones.Attributes.Add("class", "alert alert-danger alert-dismissible fade show");
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
            }


            if (string.IsNullOrEmpty(Mensaje.Trim()))
                return true;
            else
                 return false;
        }

        protected void btnRegistrarAM_Click(object sender, EventArgs e)
        {
            if (ValidarAM())
            {


            }
            else
            {
               
            }
        }
    }        
    
}