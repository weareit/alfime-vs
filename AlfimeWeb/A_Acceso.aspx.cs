﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Datos;

namespace WebApplication1
{
    public partial class Formulario_web1 : System.Web.UI.Page
    {
        //Conexión al adaptador de datos
        Datos.DataSetBDAlfimeTableAdapters.UsuarioSistemasTableAdapter ControladorAcceso = new Datos.DataSetBDAlfimeTableAdapters.UsuarioSistemasTableAdapter();
        Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter ControladorUsuario = new Datos.DataSetBDAlfimeTableAdapters.UsuariosTableAdapter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Login"] != null)
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            bool MensajeError = false;
            string Mensaje = "";
            if (txtUsuario.Text == "")
            {
                Mensaje = Mensaje + "- El Campo correspondiente al usuario se encuentra vacío!" + Environment.NewLine;
                MensajeError = true;
            }
            if (txtClave.Text == "")
            {
                Mensaje = Mensaje + "- Debe ingresar una contraseña!" + Environment.NewLine;
                MensajeError = true;
            }
            if (MensajeError)
            {
                pnlNotificaciones.Visible = true;
                lblMensaje.Text = Mensaje;
                MensajeError = false;
                Mensaje = "";
                return;
            }
            else
            {
                //Controlador acceso de usuarios al sistema
                DataTable TablaUsuarioSistema = ControladorAcceso.Tabla_Acceso_UsuarioClave(txtClave.Text.Trim(), txtUsuario.Text.Trim());
                if (TablaUsuarioSistema != null)
                {
                    if (TablaUsuarioSistema.Rows.Count > 0)
                    {
                        Session["Login"] = TablaUsuarioSistema;
                        if (Session["IdOrigen"] != null)
                        {
                            string[] Origen = (string[])Session["IdOrigen"];

                            if (Origen[0] == "Inicio")
                            {
                                DataTable Usuario = ControladorUsuario.Tabla_ConsultarUsuario(Origen[1].ToString());
                                if (Usuario != null)
                                {
                                    if (Usuario.Rows.Count > 0)
                                    {
                                        Session["Usuario"] = Usuario;
                                        //Debe devolver al perfil del usuario                                       
                                        //Response.Redirect("U_RegistroUsuario.aspx");
                                        //Debe retornar a Usuario
                                        Response.Redirect("U_Usuario.aspx");
                                    }
                                    else
                                    {
                                        Response.Redirect("Default.aspx");
                                    }
                                }
                                else
                                {
                                    Origen[0] = "Acceso";
                                    Origen[1] = "- El usuario no se encuentra registrado en el sistema!";
                                    Session["Origen"] = Origen;
                                    Response.Redirect("Default.aspx");
                                }
                            }
                            else if (Origen[0] == "Nuevo")
                            {
                                if (Convert.ToInt32(((DataTable)Session["Login"]).Rows[0][11]) < 3)
                                {
                                    Origen[0] = "Acceso";
                                    Origen[1] = "- El usuario no se encuentra registrado en el sistema!";
                                    Session["Origen"] = Origen;
                                    Response.Redirect("Default.aspx");
                                }
                                else
                                {
                                    Response.Redirect("U_RegistroUsuario.aspx");
                                }
                                
                            }
                        }
                        else
                        {
                            Response.Redirect("Default.aspx");
                        }

                    }
                    else
                    {
                        Mensaje = Mensaje + "La combinación usuario contraseña no se encuentra en el sistema, verifiquela o valide con el administrador del sistema!" + Environment.NewLine;
                        pnlNotificaciones.Visible = true;
                        lblMensaje.Text = Mensaje;
                        Mensaje = "";
                    }
                }
                else
                {
                    Mensaje = Mensaje + "La combinación usuario contraseña no se encuentra en el sistema, verifiquela o valide con el administrador del sistema!" + Environment.NewLine;                    
                    pnlNotificaciones.Visible = true;
                    lblMensaje.Text = Mensaje;
                    Mensaje = "";
                }
            }
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            Session["Origen"] = null;
            Response.Redirect("A_Registro.aspx");
        }
    }
}